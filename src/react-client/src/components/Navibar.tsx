import React ,{ useContext, useState} from "react";
import { Button, Form, Modal, Nav, Navbar , NavDropdown} from "react-bootstrap";
import { Context } from "../index";




export function Navibar(){

    const{store} = useContext(Context);

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [retryPassword, setRetryPassword] = useState('');

    const [activeLoginModal,setActiveLoginModal] = useState(false);
    const handleLoginShow =()=>setActiveLoginModal(true);
    const handleLoginClose =()=>setActiveLoginModal(false);

    const [activeRegisterModal,setActiveRegisterModal] = useState(false);
    const handleRegisterShow =()=>setActiveRegisterModal(true);
    const handleRegisterClose =()=>setActiveRegisterModal(false);

    const changeNameHandler=(e:React.ChangeEvent<HTMLInputElement>)=>{
        setName(e.target.value);
    }
    const changeEmailHandler=(e:React.ChangeEvent<HTMLInputElement>)=>{
      setEmail(e.target.value);
  }
    const changePasswordHandler=(e:React.ChangeEvent<HTMLInputElement>)=>{
      setPassword(e.target.value);
  }
    const changeRetryPasswordHandler=(e:React.ChangeEvent<HTMLInputElement>)=>{
      setRetryPassword(e.target.value);
  }

    const clickLoginHandler=(e:React.MouseEvent<HTMLButtonElement>)=>{
      store.login({email,password})
      setActiveLoginModal(false)
  }
  const clickRegisterHandler=(e:React.MouseEvent<HTMLButtonElement>)=>{
    store.register({name,email,password,retryPassword})
    setActiveRegisterModal(false)
}


    return(
        <>
        <Navbar  bg="dark" variant="dark">
          <Navbar.Brand href="#home" >
            <img
              src="../logo.png"
              height="40"
              className="me-10" 
              alt="logo"
            />
          </Navbar.Brand>

          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
                <NavDropdown  title="Courses" id="basic-nav-courses">
                    <NavDropdown.Item  href="#link">Beginner A0</NavDropdown.Item>
                    <NavDropdown.Item href="#link">Elementary A1</NavDropdown.Item>
                    <NavDropdown.Item href="#link">Intermediate B1</NavDropdown.Item>
                    <NavDropdown.Item href="#link">Upper Intermediate B2</NavDropdown.Item>
                    <NavDropdown.Item href="#link">Advanced C1</NavDropdown.Item>
                    <NavDropdown.Item href="#link">Proficiency C2</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                </NavDropdown>
            </Nav>

            <Nav>
                <Button variant="outline-info" size="sm" className="me-2" onClick={handleRegisterShow}>Register</Button>
                <Button variant="outline-info" size="sm" className="me-2"  onClick={handleLoginShow}>Log In</Button>
            </Nav>

          </Navbar.Collapse>
    
      </Navbar>

        <Modal show={activeLoginModal} onHide={handleLoginClose}>
            <Modal.Header closeButton>
                <Modal.Title>Log In</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form >
                    <Form.Group controlId="frombasicEmail">
                        <Form.Label>Email</Form.Label>
                        <Form.Control value={email} onChange={changeEmailHandler} type="email" placeholder="enter email" />
                        <Form.Text className="text-muted">We'll never sgare your email with </Form.Text>
                    </Form.Group>
                    <Form.Group controlId="frombasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control  value={password} onChange={changePasswordHandler} type="password" placeholder="enter password" />
                    </Form.Group>
                    <Form.Group controlId="frombasicCheckbox">
                        <Form.Check name="remember" type="checkbox" label="remember me" />
                    </Form.Group>
                </Form>
            </Modal.Body>
            {/* <Button  variant="info" type="submit" size="lg" onClick={clickLoginHandler} >Submit</Button> */}
            <Modal.Footer>
                <Button  variant="info"  size="lg" onClick={clickLoginHandler} >Submit</Button>
            </Modal.Footer>

        </Modal>

        <Modal show={activeRegisterModal} onHide={handleRegisterClose} >
            <Modal.Header closeButton>
                <Modal.Title>Register</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Group controlId="frombasicName">
                        <Form.Label>Name</Form.Label>
                        <Form.Control value={name} onChange={changeNameHandler} type="name" placeholder="enter your name" />
                    </Form.Group>
                    <Form.Group controlId="frombasicEmail">
                        <Form.Label>Email Address</Form.Label>
                        <Form.Control value={email} onChange={changeEmailHandler} type="email" placeholder="enter email" />
                    </Form.Group>
                    <Form.Group controlId="frombasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control value={password} onChange={changePasswordHandler} type="password" placeholder="enter password" />
                    </Form.Group>
                    <Form.Group controlId="frombasicPassword">
                        <Form.Label>Confirm Password</Form.Label>
                        <Form.Control value={retryPassword} onChange={changeRetryPasswordHandler} type="password" placeholder="repeat password" />
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button  variant="info" onClick={clickRegisterHandler} size="lg" >Create Account</Button>
            </Modal.Footer>
            
        </Modal>


    </>

    )}






