import Carousel from 'react-bootstrap/Carousel';


function CarouselStart() {
  return (
    <Carousel  variant="dark" fade style={{'height':'50%', 'width':'60%','margin':'auto'}}> 
      <Carousel.Item interval={3500} >
        <img
          className="d-block w-100"
          src="../main2.jpg"
        />
        <Carousel.Caption>
          <h2>Description</h2>
        </Carousel.Caption>
      </Carousel.Item>

      <Carousel.Item interval={3500}>
        <img
          className="d-block w-100"
          src="../course_a1.jpg"
        />

        <Carousel.Caption>
        <h2>Description</h2>
        </Carousel.Caption>
      </Carousel.Item>

      <Carousel.Item interval={3500}>
        <img
          className="d-block w-100"
          src="../course_a2.jpg"
        />

        <Carousel.Caption>
        <h2>Description</h2>
        </Carousel.Caption>
      </Carousel.Item>

      <Carousel.Item interval={3500}>
        <img
          className="d-block w-100"
          src="../main1.jpg"
        />

        <Carousel.Caption>
        <h2>Description</h2>
        </Carousel.Caption>
      </Carousel.Item>


    </Carousel>
  );
}

export default CarouselStart;