import React, { useState } from 'react';
import CarouselStart from '../components/CarouselStart';
import { Navibar } from '../components/Navibar';



function StartPage() {

  return (
    <>
      <Navibar />
      <div style={{padding:'30px'}}>
        <CarouselStart />
      </div>
    </>
  );
}

export default StartPage;
