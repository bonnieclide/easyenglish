import { AxiosResponse } from "axios"
import $api from "../http"
import { AuthResponse } from "../models/auth/AuthResponse"
import { IUser } from "../models/IUser"

export default class UserService{

    static fetchCurrentUser():Promise<AxiosResponse<IUser>>{
        return $api.get<IUser>('/GetCurrentUser')
    }


    static fetchUsers():Promise<AxiosResponse<IUser[]>>{
        return $api.get<IUser[]>('/GetUsers')
    }



}