import { AxiosResponse } from "axios"
import $api from "../http"
import { AuthRequest } from "../models/auth/AuthRequest"
import { AuthResponse } from "../models/auth/AuthResponse"
import { RegisterRequest } from "../models/auth/RegisterRequest"




export default class AuthService{

    static async login (params:AuthRequest): Promise<AxiosResponse<AuthResponse>>{
        return $api.post<AuthResponse>('/Login',params)
    }

    static async register (params:RegisterRequest): Promise<void>{
        return $api.post('/Register',params)
    }

    static async logout (): Promise<void>{
        return $api.post('/Logout')
    }

    // RefreshToken


}