export interface IUser{
    id:number
    name:string,
    email:string,
    role: Role
}

enum Role{
    Admin,
    Metodist,
    User
}