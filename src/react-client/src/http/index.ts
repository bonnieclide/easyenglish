import axios from "axios";

export const API_URL = 'https://localhost:7056'

const $api =axios.create({
    headers:{
        'Content-Type' : 'application/json; charset=UTF-8'
    },
    withCredentials: false,
    baseURL:API_URL
})


$api.interceptors.request.use((config)=>{
    config.headers!.Authorization =`Bearer ${localStorage.getItem('token')}`
    return config;
})


export default $api;