import { makeAutoObservable } from "mobx";
import { AuthRequest } from "../models/auth/AuthRequest";
import { RegisterRequest } from "../models/auth/RegisterRequest";
import { IUser } from "../models/IUser";
import AuthService from "../services/AuthService";
import axios from "axios";
import { AuthResponse } from "../models/auth/AuthResponse";
import { API_URL } from "../http";

export default class Store{
    user={} as IUser;
    isAuth=false;

    constructor(){
        makeAutoObservable(this);
    }

    setAuth(bool:boolean){
        this.isAuth=bool;
    }
    setUser(user:IUser){
        this.user=user;
    }


    async login(request:AuthRequest){
        try{
            const response = await AuthService.login(request);
            console.log(response) // FOR TEST ----------
            localStorage.setItem('token',response.data.accessToken);
            this.setAuth(true);
        } catch(e){
            // console.log(e.response?.data?.message);
        }
    }

    async register(request:RegisterRequest){
        try{
            const response = await AuthService.register(request);
        } catch(e){
            // console.log(e.response?.data?.message);
        }
    }

    async logout(){
        try{
            const response = await AuthService.logout();
            localStorage.removeItem('token');
            this.setAuth(false);
        } catch(e){
            // console.log(e.response?.data?.message);
        }
    }

    async checkAuth(){
        try{
            const response = await axios.get<AuthResponse>('${API_URL}/RefreshToken',{withCredentials:true});
            console.log(response) // FOR TEST ----------
            localStorage.setItem('token',response.data.accessToken);
            this.setAuth(true);

        } catch(e){
            // console.log(e.response?.data?.message);
        }
    }






}