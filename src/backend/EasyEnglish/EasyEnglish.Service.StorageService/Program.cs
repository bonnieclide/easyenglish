using EasyEnglish.Platform.Brokers;
using EasyEnglish.Service.StorageService.Cache;
using EasyEnglish.Service.StorageService.Infrastructure;
using EasyEnglish.Service.StorageService.Middlewares;
using EasyEnglish.Service.StorageService.Options;
using EasyEnglish.Service.StorageService.Services;
using NpgsqlTypes;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

builder.Services.Configure<ConnectDbConfig>(builder.Configuration.GetSection("ConnectionStrings"));

builder.Services.AddSingleton<ICourseManager, CourseManager>();
builder.Services.AddSingleton<IModuleManager, ModuleManager>();
builder.Services.AddSingleton<ILessonManager, LessonManager>();
builder.Services.AddSingleton<ICourseRepository, CourseRepository>();
builder.Services.AddSingleton<IModuleRepository, ModuleRepository>();
builder.Services.AddSingleton<ILessonRepository, LessonRepository>();
builder.Services.AddSingleton<ICacheManager, CourseCacheManager>();

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddStackExchangeRedisCache(options =>
{
    options.Configuration = "eu1-above-sparrow-38082.upstash.io:38082,password=fd5c7c1ba52541918e98a3dc01cbd1a8";
});

builder.Host.UseCustomSerilog();

var app = builder.Build();

app.UseMiddleware<ExceptionHandlingMiddleware>();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();