﻿namespace EasyEnglish.Service.StorageService.Cache;

public interface ICacheManager
{
    Task AddAsync<T>(Guid guid, T value);
    Task<T?> GetAsync<T>(Guid guid);
}