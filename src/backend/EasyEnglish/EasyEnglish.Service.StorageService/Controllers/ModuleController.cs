﻿using EasyEnglish.Service.StorageService.Services;
using Microsoft.AspNetCore.Mvc;

namespace EasyEnglish.Service.StorageService.Controllers;

[ApiController]
[Route("[controller]")]
public class ModuleController : Controller
{
    private readonly ILogger<ModuleController> _logger;
    private readonly IModuleManager _moduleManager;

    public ModuleController(ILogger<ModuleController> logger, IModuleManager moduleManager)
    {
        _logger = logger;
        _moduleManager = moduleManager;
    }
    
    [HttpGet]
    [Route(nameof(GetModule))]
    public async Task<ActionResult> GetModule(Guid guid)
    {
        var module = await _moduleManager.GetModule(guid);
        
        if (module is null)
            return BadRequest();
        
        _logger.LogInformation("Find module with id: {0}", module.Id);
        return Ok(module);
    }
}