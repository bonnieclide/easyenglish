using EasyEnglish.Service.StorageService.Domain;
using EasyEnglish.Service.StorageService.Services;
using Microsoft.AspNetCore.Mvc;

namespace EasyEnglish.Service.StorageService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CourseController : Controller
    {
        private readonly ILogger<CourseController> _logger;
        private readonly ICourseManager _courseManager;

        public CourseController(ILogger<CourseController> logger, ICourseManager courseManager)
        {
            _logger = logger;
            _courseManager = courseManager;
        }

        [HttpGet]
        [Route(nameof(GetCourse))]
        public async Task<ActionResult<Course>> GetCourse(Guid guid)
        {
            var cource = await _courseManager.GetCourse(guid);
        
            if (cource is null)
                return BadRequest();
        
            _logger.LogInformation("Find course with guid: {0}", guid);
            return Ok(cource);
        }

        [HttpGet]
        [Route(nameof(GetAllCourses))]
        public async Task<ActionResult<Course[]>> GetAllCourses()
        {
            var courses = await _courseManager.GetAllCourses();
        
            _logger.LogInformation("Find all courses!");
            return Ok(courses);
        }
    }
}