﻿using EasyEnglish.Service.StorageService.Domain.Lessons;
using EasyEnglish.Service.StorageService.Services;
using Microsoft.AspNetCore.Mvc;

namespace EasyEnglish.Service.StorageService.Controllers;

[ApiController]
[Route("[controller]")]
public class LessonController : Controller
{
    private readonly ILogger<LessonController> _logger;
    private readonly ILessonManager _lessonManager;

    public LessonController(ILogger<LessonController> logger, ILessonManager lessonManager)
    {
        _logger = logger;
        _lessonManager = lessonManager;
    }
    
    [HttpGet]
    [Route(nameof(GetLesson))]
    public async Task<object?> GetLesson(Guid guid, string lessonType = "")
    {
        Enum.TryParse<LessonType>(lessonType, out var lessonTypeEnum);
        
        return lessonTypeEnum switch
        {
            LessonType.Theory => await _lessonManager.GetLesson<TheoryLesson>(guid),
            LessonType.Quiz => await _lessonManager.GetLesson<QuizLesson>(guid),
            LessonType.Audio => await _lessonManager.GetLesson<AudioLesson>(guid),
            _ => throw new Exception()
        };
    }
    
    [HttpGet]
    [Route(nameof(GetAllLessons))]
    public async Task<IEnumerable<object>> GetAllLessons(string lessonType = "")
    {
        Enum.TryParse<LessonType>(lessonType, out var lessonTypeEnum);
        
        return lessonTypeEnum switch
        {
            LessonType.Theory => await _lessonManager.GetAllLessons<TheoryLesson>(),
            LessonType.Quiz => await _lessonManager.GetAllLessons<QuizLesson>(),
            LessonType.Audio => await _lessonManager.GetAllLessons<AudioLesson>(),
            _ => throw new Exception()
        };
    }
}