﻿using System.Text.Json.Serialization;

namespace EasyEnglish.Service.StorageService.Domain.Lessons;

public sealed class AudioLesson : Entity, ILesson
{
    [JsonInclude]
    public string Title { get; private set; }
    
    [JsonInclude]
    public string Question { get; private set; }
    
    [JsonInclude]
    public string Answer { get; private set; }
    
    [JsonInclude]
    public Guid FileGuid { get; private set; }
    
    [JsonInclude]
    public bool IsFree { get; private set; }
    
    [JsonInclude]
    public string Description { get; private set; }

    public string LessonType => "Audio";

    public AudioLesson SetFileGuid(Guid guid)
    {
        FileGuid = guid;
        return this;
    }
    
    public AudioLesson SetQuestion(string question)
    {
        Question = question;
        return this;
    }

    public AudioLesson SetAnswer(string answer)
    {
        Answer = answer;
        return this;
    }
    
    public AudioLesson SetIsFree(bool isFree)
    {
        IsFree = isFree;
        return this;
    }
    
    public AudioLesson SetTitle(string title)
    {
        Title = title;
        return this;
    }
    
    public AudioLesson SetDescription(string description)
    {
        Description = description;
        return this;
    }
}