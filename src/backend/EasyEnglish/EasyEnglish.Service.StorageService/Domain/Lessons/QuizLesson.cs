﻿namespace EasyEnglish.Service.StorageService.Domain.Lessons;

public sealed class QuizLesson : Entity, ILesson
{
    public string Title { get; private set; }
    public string Question { get; private set; }
    public string Answer { get; private set; }
    public bool IsFree { get; private set; }
    public string Description { get; private set; }
    public string[] Options { get; private set; }

    public string LessonType => "Quiz";
    
    public QuizLesson AddOptions(IReadOnlyCollection<string> options)
    {
        Options = options.ToArray();
        return this;
    }
    
    public QuizLesson SetQuestion(string question)
    {
        Question = question;
        return this;
    }
    
    public QuizLesson SetAnswer(string answer)
    {
        Answer = answer;
        return this;
    }
    
    public QuizLesson SetIsFree(bool isFree)
    {
        IsFree = isFree;
        return this;
    }
    
    public QuizLesson SetTitle(string title)
    {
        Title = title;
        return this;
    }
    
    public QuizLesson SetDescription(string description)
    {
        Description = description;
        return this;
    }
}