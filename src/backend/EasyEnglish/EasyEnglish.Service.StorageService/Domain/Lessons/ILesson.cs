﻿namespace EasyEnglish.Service.StorageService.Domain.Lessons;

public interface ILesson
{
    long Id { get; }
    Guid Guid { get; }
    string LessonType { get; }
}