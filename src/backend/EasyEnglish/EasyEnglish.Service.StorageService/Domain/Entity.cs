﻿using System.Text.Json.Serialization;

namespace EasyEnglish.Service.StorageService.Domain
{
    public class Entity
    {
        public long Id { get; private set; }
        public Guid Guid { get; private set; } 
    }
}
