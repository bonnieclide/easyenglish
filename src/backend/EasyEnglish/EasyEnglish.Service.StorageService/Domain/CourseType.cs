﻿namespace EasyEnglish.Service.StorageService.Domain;

public class CourseType
{
    public int Id { get; private set; }
    public string Name { get; set; } = "English";
}