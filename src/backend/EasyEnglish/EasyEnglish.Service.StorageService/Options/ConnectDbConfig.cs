﻿namespace EasyEnglish.Service.StorageService.Options;

public class ConnectDbConfig
{
    public string? PostgreConnectionString { get; set; }
}