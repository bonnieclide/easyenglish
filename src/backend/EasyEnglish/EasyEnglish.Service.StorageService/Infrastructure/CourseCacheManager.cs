﻿using System.Text.Json;
using EasyEnglish.Service.StorageService.Cache;
using Microsoft.Extensions.Caching.Distributed;

namespace EasyEnglish.Service.StorageService.Infrastructure;

public class CourseCacheManager : ICacheManager
{
    private readonly IDistributedCache _cache;

    public CourseCacheManager(IDistributedCache cache)
    {
        _cache = cache;
    }

    public async Task AddAsync<T>(Guid guid, T value)
    {
        await _cache.SetStringAsync(guid.ToString(),
            JsonSerializer.Serialize(value),
            new DistributedCacheEntryOptions
            {
                SlidingExpiration = TimeSpan.FromMinutes(60)
            });
    }

    public async Task<T?> GetAsync<T>(Guid guid)
    {
        //TODO добавить механизм чтения из кэша
        return default;
        
        try
        {
            var cacheResult = await _cache.GetStringAsync(guid.ToString());
            return JsonSerializer.Deserialize<T>(cacheResult);
        }
        catch
        {
            return default;
        }
    }
}