﻿using Dapper;
using EasyEnglish.Service.StorageService.Domain.Lessons;
using EasyEnglish.Service.StorageService.Options;
using EasyEnglish.Service.StorageService.Services;
using Microsoft.Extensions.Options;
using Npgsql;

namespace EasyEnglish.Service.StorageService.Infrastructure;

public class LessonRepository : ILessonRepository
{
    private readonly string? _connectionString;
    private readonly SemaphoreSlim _semaphore = new(15, 15);

    public LessonRepository(IOptions<ConnectDbConfig> sensorsPoolConfig)
    {
        _connectionString = sensorsPoolConfig.Value.PostgreConnectionString;
    }
    
    public async Task<T> GetLesson<T>(Guid guid)
    {
        var tableName = GetTableNameByType(typeof(T));
        
        var sqlPattern = $"select * from {tableName} where Guid = @Guid";

        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                Guid = guid,
            };

            return await connection.QueryFirstOrDefaultAsync<T>(sqlPattern, param);
        }
        finally
        {
            _semaphore.Release();
        }
    }

    public async Task<IEnumerable<T>> GetAllLessons<T>()
    {
        var tableName = GetTableNameByType(typeof(T));
        
        var sqlPattern = $"select * from {tableName} where Id = @Id";

        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            return await connection.QueryAsync<T>(sqlPattern);
        }
        finally
        {
            _semaphore.Release();
        }
    }

    private string GetTableNameByType(Type type)
    {
        if(typeof(TheoryLesson) == type) 
            return@"theoryLesson";
        
        if(typeof(QuizLesson) == type) 
            return "quizLesson";
        
        if(typeof(AudioLesson) == type) 
            return "audioLesson";
        
        //TODO: Добавить пользовательское исключение
        throw new Exception();
    }
}