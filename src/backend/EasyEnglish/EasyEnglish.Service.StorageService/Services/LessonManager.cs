﻿using System.Text.Json;
using EasyEnglish.Service.StorageService.Cache;
using EasyEnglish.Service.StorageService.Domain.Lessons;
using Microsoft.Extensions.Caching.Distributed;

namespace EasyEnglish.Service.StorageService.Services;

public class LessonManager : ILessonManager
{
    private readonly ILessonRepository _lessonRepository;
    private readonly ICacheManager _cache;

    public LessonManager(ILessonRepository lessonRepository, ICacheManager cache)
    {
        _lessonRepository = lessonRepository;
        _cache = cache;
    }

    public async Task<T?> GetLesson<T>(Guid guid)
    {
        var cacheResult = await _cache.GetAsync<T>(guid);

        if (cacheResult?.Equals(default) == null)
        {
            return await GetFromDb<T>(guid);
        }

        return cacheResult;
    }

    public async Task<IEnumerable<T>> GetAllLessons<T>()
    {
        return await _lessonRepository.GetAllLessons<T>();
    }

    private async Task<T> GetFromDb<T>(Guid guid)
    {
        var lessonFromDb = await _lessonRepository.GetLesson<T>(guid);

        if (lessonFromDb != null)
        {
            await _cache.AddAsync(guid, lessonFromDb);
        }

        return lessonFromDb;
    }
}