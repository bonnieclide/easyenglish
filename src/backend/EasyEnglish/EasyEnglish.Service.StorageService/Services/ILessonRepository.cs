﻿namespace EasyEnglish.Service.StorageService.Services;

public interface ILessonRepository
{
    Task<T> GetLesson<T>(Guid guid);
    Task<IEnumerable<T>> GetAllLessons<T>();
}