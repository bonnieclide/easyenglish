﻿using EasyEnglish.Service.StorageService.Cache;
using EasyEnglish.Service.StorageService.Domain;

namespace EasyEnglish.Service.StorageService.Services;

public class ModuleManager : IModuleManager
{
    private readonly IModuleRepository _moduleRepository;
    private readonly ICacheManager _cache;

    public ModuleManager(IModuleRepository moduleRepository, ICacheManager cache)
    {
        _moduleRepository = moduleRepository;
        _cache = cache;
    }

    public async Task<Module?> GetModule(Guid guid)
    {
        var cacheResult = await _cache.GetAsync<Module?>(guid);

        if (cacheResult?.Equals(default) == null)
        {
            var moduleFromDb = await _moduleRepository.GetModule(guid);
                
            if (moduleFromDb != null)
            {
                await _cache.AddAsync(guid, moduleFromDb);
            }
            return moduleFromDb;
        }

        return cacheResult;
    }
}