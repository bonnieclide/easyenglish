﻿using EasyEnglish.Service.StorageService.Cache;
using EasyEnglish.Service.StorageService.Domain;

namespace EasyEnglish.Service.StorageService.Services
{
    public class CourseManager : ICourseManager
    {
        private readonly ICourseRepository _courseRepository;
        private readonly ICacheManager _cache;

        public CourseManager(ICourseRepository courseRepository, ICacheManager cache)
        {
            _courseRepository = courseRepository;
            _cache = cache;
        }

        public async Task<Course[]> GetAllCourses()
        {
            return await _courseRepository.GetAllCourses();
        }

        public async Task<Course?> GetCourse(Guid guid)
        {
            var cacheResult = await _cache.GetAsync<Course?>(guid);

            if (cacheResult?.Equals(default) == null)
            {
                var courseFromDb = await _courseRepository.GetCourse(guid);
                
                if (courseFromDb != null)
                {
                    await _cache.AddAsync(guid, courseFromDb);
                }
                return courseFromDb;
            }

            return cacheResult;
        }
    }
}
