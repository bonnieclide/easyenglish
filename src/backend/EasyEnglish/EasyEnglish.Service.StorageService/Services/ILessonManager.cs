﻿using EasyEnglish.Service.StorageService.Domain.Lessons;

namespace EasyEnglish.Service.StorageService.Services;

public interface ILessonManager
{
    Task<T?> GetLesson<T>(Guid guid);
    Task<IEnumerable<T>> GetAllLessons<T>();
}