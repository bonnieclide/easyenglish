﻿using EasyEnglish.Service.StorageService.Domain;

namespace EasyEnglish.Service.StorageService.Services;

public interface IModuleRepository
{
    Task<Module?> GetModule(Guid guid);
}