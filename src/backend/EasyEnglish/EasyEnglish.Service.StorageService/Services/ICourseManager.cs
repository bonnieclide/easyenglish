﻿using EasyEnglish.Service.StorageService.Domain;

namespace EasyEnglish.Service.StorageService.Services;

public interface ICourseManager
{
    Task<Course?> GetCourse(Guid guid);
    Task<Course[]> GetAllCourses();
}
