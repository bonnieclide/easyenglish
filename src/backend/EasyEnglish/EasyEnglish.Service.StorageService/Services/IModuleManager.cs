﻿using EasyEnglish.Service.StorageService.Domain;

namespace EasyEnglish.Service.StorageService.Services;

public interface IModuleManager
{
    Task<Module?> GetModule(Guid guid);
}