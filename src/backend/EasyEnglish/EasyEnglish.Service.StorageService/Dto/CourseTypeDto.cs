﻿using System.Text.Json.Serialization;

namespace EasyEnglish.Service.StorageService.Dto;

public class CourseTypeDto
{
    [JsonPropertyName("name")]
    public string? Name { get; set; }
}