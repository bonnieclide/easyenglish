﻿using System.Text.Json.Serialization;

namespace EasyEnglish.Service.StorageService.Dto;

public class LessonDto
{
    [JsonPropertyName("guid")]
    public Guid? Guid { get; set; }
    
    [JsonPropertyName("name")]
    public string? Name { get; set; }
    
    [JsonPropertyName("description")]
    public string? Description { get; set; }
    
    [JsonPropertyName("isFree")]
    public bool? IsFree { get; set; }
    
    [JsonPropertyName("lessonType")]
    public string LessonType { get; set; }
    
    [JsonPropertyName("title")]
    public string? Title { get; set; }
    
    [JsonPropertyName("question")]
    public string? Question { get; set; }
    
    [JsonPropertyName("answer")]
    public string? Answer { get; set; }
    
    [JsonPropertyName("fileGuid")]
    public Guid? FileGuid { get; set; }
}