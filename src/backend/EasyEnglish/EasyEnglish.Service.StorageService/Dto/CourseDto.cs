﻿using System.Text.Json.Serialization;

namespace EasyEnglish.Service.StorageService.Dto;

public class CourseDto
{
    [JsonPropertyName("guid")]
    public Guid? Guid { get; set; }
    
    [JsonPropertyName("name")]
    public string? Name { get; set; }
    
    [JsonPropertyName("description")]
    public string? Description { get; set; }
    
    [JsonPropertyName("courseType")]
    public CourseTypeDto? CourseType { get; set; }
    
    [JsonPropertyName("courseLevel")]
    public CourseLevelDto? CourseLevel { get; set; }
    
    [JsonPropertyName("isFree")]
    public bool? IsFree { get; set; }
    
    [JsonPropertyName("modules")]
    public IReadOnlyCollection<ModuleDto>? Modules { get; set; }
}