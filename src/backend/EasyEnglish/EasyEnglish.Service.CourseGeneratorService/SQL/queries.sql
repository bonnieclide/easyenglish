﻿select * from courseLevel;

select * from courseType;

select * from course;
select * from module;
select * from quizLesson;
select * from theorylesson;
select * from audiolesson;

select * from lesson_to_module;
select * from module_to_course;

select * from quizlesson where Id = 1;

select course.id, course.guid, course.name, course.description, course.isFree,
       level.id, level.name,
       type.id, type.name
from course
         inner join courseType as type on type.id = course.courseTypeId
         inner join courseLevel as level on level.id = course.courseLevelId;

select * from module
                  inner join lessonlink as theory on theory.moduleguid = 'c4e359e1-baf0-4a2d-ac95-4c20ab3700c10'
                  inner join theorylesson t on lessonguid = t.guid

where module.Guid = 'c4e359e1-baf0-4a2d-ac95-4c20ab3700c1';

select course.id, course.guid, course.name, course.description, course.isfree,
       m.id, m.guid
from course
         left join module_to_course as l on l.courseguid = 'ebe81d4b-e560-48ed-8ec4-7f20091a3a14'
         left join module as m on l.courseguid = m.guid
where course.Guid = 'ebe81d4b-e560-48ed-8ec4-7f20091a3a14';

select course.id, course.guid, course.name, course.description, course.isfree,
       type.id, type.name,
       level.id, level.name,
       m.id, m.guid, m.description, m.isfree, m.name,
       t.id, t.guid, t.isfree, t.description, t.text, t.title,
       a.id, a.guid, a.isfree, a.description, a.fileguid, a.title, a.question, a.answer,
       q.id, q.guid, q.isfree, q.description, q.options, q.question, q.answer
from course
         left join courseType as type on course.coursetypeid = type.id
         left join courselevel as level on course.courselevelid = level.id
         left join module_to_course as l on l.courseguid = '4f109dd3-eabd-4ac9-961f-34a207445b90'
         left join module as m on l.moduleguid = m.guid
         left join lesson_to_module as lm on lm.moduleguid = m.guid
         left join theorylesson as t on lm.lessonguid = t.guid
         left join audiolesson as a on lm.lessonguid = a.guid
         left join quizlesson as q on lm.lessonguid = q.guid
where course.Guid = '4f109dd3-eabd-4ac9-961f-34a207445b90' order by m.id;

select course.id, course.guid, course.name, course.description, course.isfree,
       type.id, type.name,
       level.id, level.name,
       m.id, m.guid, m.description, m.isfree, m.name,
       t.id, t.guid, t.isfree, t.description, t.text, t.title,
       a.id, a.guid, a.isfree, a.description, a.fileguid, a.title, a.question, a.answer,
       q.id, q.guid, q.isfree, q.description, q.options, q.question, q.answer
from course
         left join courseType as type on course.coursetypeid = type.id
         left join courselevel as level on course.courselevelid = level.id
         left join module_to_course as l on l.courseguid = course.guid
         left join module as m on l.moduleguid = m.guid
         left join lesson_to_module as lm on lm.moduleguid = m.guid
         left join theorylesson as t on lm.lessonguid = t.guid
         left join audiolesson as a on lm.lessonguid = a.guid
         left join quizlesson as q on lm.lessonguid = q.guid
order by course.id, m.id desc;


