﻿drop table if exists course;
drop table if exists courseLevel;
drop table if exists courseType;
drop table if exists audioLesson;
drop table if exists theoryLesson;
drop table if exists quizLesson;
drop table if exists module_to_course;
drop table if exists lesson_to_module;
drop table if exists module_to_course;
drop table if exists module;

create table courseLevel
(
    id bigserial primary key,
    name text not null unique
);

create index on courseLevel (name);

create table courseType
(
    id bigserial primary key,
    name text not null unique
);

create index on courseType (name);

create table module_to_course
(
    id bigserial primary key,
    moduleGuid uuid,
    courseGuid uuid
);

create table lesson_to_module
(
    id bigserial primary key,
    lessonGuid uuid,
    moduleGuid uuid
);

create table audioLesson
(
    id bigserial primary key,
    guid uuid default uuid_generate_v4 (),
    fileGuid uuid,
    title text not null,
    question text not null,
    answer text not null,
    description text,
    isFree bool default true,
    lessonType text not null
);

create table theoryLesson
(
    id bigserial primary key,
    guid uuid default uuid_generate_v4 (),
    title text not null,
    text text not null,
    description text,
    isFree bool default true,
    lessonType text not null
);

create table quizLesson
(
    id bigserial primary key,
    guid uuid default uuid_generate_v4 (),
    title text not null,
    question text not null,
    answer text not null,
    description text,
    options text[] not null,
    isFree bool default true,
    lessonType text not null
);

create table module
(
    id bigserial primary key,
    guid uuid unique default uuid_generate_v4 (),
    name text not null,
    description text,
    isFree bool default true
);

create table course
(
    id bigserial primary key,
    guid uuid default uuid_generate_v4 (),
    name text not null,
    description text,
    courseTypeId int not null references courseType(id),
    courseLevelId int not null references courseLevel(id),
    isFree bool default true  
);

insert into courseLevel
values (1, 'A1'), (2, 'A2'), (3, 'B1'), (4, 'B2'), (5, 'C1'), (6, 'C2');

insert into courseType
values (1, 'English');

select * from courseLevel;

select * from courseType;

select * from course;
select * from module;
select * from quizLesson;

select course.id, course.guid, course.name, course.description, course.isFree,
       level.id, level.name,
       type.id, type.name
from course
         inner join courseType as type on type.id = course.courseTypeId
         inner join courseLevel as level on level.id = course.courseLevelId;