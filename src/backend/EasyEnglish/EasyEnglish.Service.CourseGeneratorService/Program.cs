using EasyEnglish.Platform.Brokers;
using EasyEnglish.Service.CourseGeneratorService.Infrastructure;
using EasyEnglish.Service.CourseGeneratorService.Middlewares;
using EasyEnglish.Service.CourseGeneratorService.Options;
using EasyEnglish.Service.CourseGeneratorService.Services;
using EasyEnglish.Service.CourseGeneratorService.Domain;
using EasyEnglish.Service.CourseGeneratorService.Services.Dto;
using EasyEnglish.Service.CourseGeneratorService.Validators;
using FluentValidation;
using FluentValidation.AspNetCore;

namespace EasyEnglish.Service.CourseGeneratorService;

using CourseGeneratorService.Middlewares;

    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            
            builder.Services.Configure<ConnectDbConfig>(builder.Configuration.GetSection("ConnectionStrings"));

            builder.Services.AddSingleton<ICourseManager, CourseManager>();
            builder.Services.AddSingleton<IModuleManager, ModuleManager>();
            builder.Services.AddSingleton<ILessonManager, LessonManager>();
            builder.Services.AddSingleton<ICourseRepository, CourseRepository>();
            builder.Services.AddSingleton<IModuleRepository, ModuleRepository>();
            builder.Services.AddSingleton<ILessonRepository, LessonRepository>();

            builder.Services.AddControllers();
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            builder.Services.AddFluentValidationAutoValidation();
            
            builder.Services.AddScoped<IValidator<AudioLessonData>, AudioLessonDataValidator>();
            builder.Services.AddScoped<IValidator<QuizLessonData>, QuizLessonDataValidator>();
            builder.Services.AddScoped<IValidator<TheoryLessonData>, TheoryLessonDataValidator>();
            
            builder.Host.UseCustomSerilog();

            var app = builder.Build();

            app.UseMiddleware<ExceptionHandlingMiddleware>();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
