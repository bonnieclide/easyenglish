﻿using Confluent.Kafka;
using EasyEnglish.Platform.Brokers;

namespace EasyEnglish.Service.CourseGeneratorService.Services;

public class KafkaProducer : IDisposable
{
    readonly IProducer<Guid, object> _producer = new KafkaProducerBuilder<Guid, object>().Build();

    public async Task<Guid> SendFile(byte[] file, string fileName)
    {
        var fileGuid = Guid.NewGuid();

        await _producer.ProduceAsync("easyenglish-add-file",
            new Message<Guid, object>
            {
                Key = Guid.NewGuid(),
                Value = new
                {
                    Guid = fileGuid,
                    Name = fileName,
                    Data = file
                }
            });

        return fileGuid;
    }

    public void Dispose()
    {
        _producer.Dispose();
    }
}