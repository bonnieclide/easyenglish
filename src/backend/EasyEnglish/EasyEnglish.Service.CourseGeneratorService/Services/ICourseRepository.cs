﻿using EasyEnglish.Service.CourseGeneratorService.Domain;

namespace EasyEnglish.Service.CourseGeneratorService.Services
{
    public interface ICourseRepository
    {
        Task<Guid> AddCourse(Course course);
        Task DeleteCourse(Guid guid);
        Task<Course?> GetCourse(Guid guid);
        Task<Course[]> GetAllCourses();
        Task AddModule(Guid courseGuid, Guid moduleGuid);
        Task DeleteModule(Guid courseGuid, Guid moduleGuid);
    }
}
