﻿using EasyEnglish.Service.CourseGeneratorService.Services.Dto;

namespace EasyEnglish.Service.CourseGeneratorService.Services;

using CourseGeneratorService.Domain;

public interface ICourseManager
{
    Task<Guid> CreateCourse(CourseData courseData);
    Task<Course?> GetCourse(Guid guid);
    Task<Course[]> GetAllCourses();
    Task DeleteCourse(Guid guid);
    Task AddModuleInCourse(Guid courseGuid, Guid moduleGuid);
    Task DeleteModuleFromCourse(Guid courseGuid, Guid moduleGuid);
}
