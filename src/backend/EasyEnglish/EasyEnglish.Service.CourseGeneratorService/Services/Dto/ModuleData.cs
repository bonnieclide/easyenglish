﻿namespace EasyEnglish.Service.CourseGeneratorService.Services.Dto;

public class ModuleData
{
    public string Name { get; set; }
    
    public string Description { get; set; }
    
    public bool IsFree { get; set; }
}