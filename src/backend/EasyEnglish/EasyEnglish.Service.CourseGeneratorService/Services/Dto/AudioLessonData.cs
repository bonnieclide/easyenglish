﻿namespace EasyEnglish.Service.CourseGeneratorService.Services.Dto;

public class AudioLessonData
{
    public string Title { get; set; }
    public string Question { get; set; }
    public string Answer { get; set; }
    public bool IsFree { get; set; }
    public string Description { get; set; }
    public IFormFile File { get; set; }
}