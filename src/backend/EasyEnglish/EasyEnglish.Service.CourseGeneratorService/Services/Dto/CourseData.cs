﻿namespace EasyEnglish.Service.CourseGeneratorService.Services.Dto;

public class CourseData
{
    public string Name { get; set; } 
    public string CourseType { get; set; } 
    public string CourseLevel { get; set; } 
    public bool IsFree { get; set; }
    public string Description { get; set; }
}