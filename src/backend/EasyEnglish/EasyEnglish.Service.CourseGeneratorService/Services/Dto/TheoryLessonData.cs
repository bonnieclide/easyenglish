﻿namespace EasyEnglish.Service.CourseGeneratorService.Services.Dto;

public class TheoryLessonData
{
    public string Text { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public bool IsFree { get; set; }
}