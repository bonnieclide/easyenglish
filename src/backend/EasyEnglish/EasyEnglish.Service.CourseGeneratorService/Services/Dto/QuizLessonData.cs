﻿namespace EasyEnglish.Service.CourseGeneratorService.Services.Dto;

public class QuizLessonData
{
    public string Title { get; set; }
    public string Question { get; set; }
    public string Answer { get; set; }
    public bool IsFree { get; set; }
    public string Description { get; set; }
    public string[] Options { get; set; }
}