﻿using EasyEnglish.Service.CourseGeneratorService.Domain;
using EasyEnglish.Service.CourseGeneratorService.Services.Dto;

namespace EasyEnglish.Service.CourseGeneratorService.Services;

public class ModuleManager : IModuleManager
{
    private readonly IModuleRepository _moduleRepository;

    public ModuleManager(IModuleRepository moduleRepository)
    {
        _moduleRepository = moduleRepository;
    }

    public async Task<Guid> CreateModule(ModuleData moduleData)
    {
        return await _moduleRepository.AddModule(moduleData.Name, moduleData.Description, moduleData.IsFree);
    }

    public async Task DeleteModule(Guid guid)
    {
        await _moduleRepository.DeleteModule(guid);
    }

    public async Task AddLessonIntoModule(Guid moduleGuid, Guid lessonGuid)
    {
        await _moduleRepository.AddLessonIntoModule(moduleGuid, lessonGuid);
    }

    public async Task DeleteLessonFromModule(Guid moduleGuid, Guid lessonGuid)
    {
        await _moduleRepository.DeleteLessonFromModule(moduleGuid, lessonGuid);
    }

    public async Task<Module?> GetModule(Guid guid)
    {
        return await _moduleRepository.GetModule(guid);
    }
}