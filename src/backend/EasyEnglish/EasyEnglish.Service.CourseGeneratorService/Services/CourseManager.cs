﻿using EasyEnglish.Service.CourseGeneratorService.Domain;
using EasyEnglish.Service.CourseGeneratorService.Services.Dto;

namespace EasyEnglish.Service.CourseGeneratorService.Services
{
    public class CourseManager : ICourseManager
    {
        private readonly ICourseRepository _courseRepository;

        public CourseManager(ICourseRepository courseRepository)
        {
            _courseRepository = courseRepository;
        }

        public async Task<Guid> CreateCourse(CourseData courseData)
        {
            var course = new Course()
                .SetName(courseData.Name)
                .SetCourseType(new CourseType{ Name = courseData.CourseType })
                .SetCourseLevel(new CourseLevel{ Name = courseData.CourseLevel })
                .SetIsFree(courseData.IsFree)
                .SetDescription(courseData.Description);
            
            return await _courseRepository.AddCourse(course);
        }

        public async Task<Course[]> GetAllCourses()
        {
            return await _courseRepository.GetAllCourses();
        }

        public async Task DeleteCourse(Guid guid)
        {
            await _courseRepository.DeleteCourse(guid);
        }

        public async Task AddModuleInCourse(Guid courseGuid, Guid moduleGuid)
        {
            await _courseRepository.AddModule(courseGuid, moduleGuid);
        }

        public async Task DeleteModuleFromCourse(Guid courseGuid, Guid moduleGuid)
        {
            await _courseRepository.DeleteModule(courseGuid, moduleGuid);
        }

        public async Task<Course?> GetCourse(Guid guid)
        {
            return await _courseRepository.GetCourse(guid);
        }
    }
}
