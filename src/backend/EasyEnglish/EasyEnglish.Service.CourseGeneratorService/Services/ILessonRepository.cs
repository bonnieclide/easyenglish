﻿using EasyEnglish.Service.CourseGeneratorService.Domain;

namespace EasyEnglish.Service.CourseGeneratorService.Services;

public interface ILessonRepository
{
    Task<Guid> AddTheoryLesson(TheoryLesson lesson);
    Task<Guid> AddQuizLesson(QuizLesson lesson);
    Task<Guid> AddAudioLesson(AudioLesson lesson);
    Task<T> GetLesson<T>(Guid guid);
    Task<IEnumerable<T>> GetAllLessons<T>();
    Task DeleteLesson<T>(Guid guid);
}