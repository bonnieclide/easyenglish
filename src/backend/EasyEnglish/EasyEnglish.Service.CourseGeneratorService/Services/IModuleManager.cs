﻿using EasyEnglish.Service.CourseGeneratorService.Domain;
using EasyEnglish.Service.CourseGeneratorService.Services.Dto;

namespace EasyEnglish.Service.CourseGeneratorService.Services;

public interface IModuleManager
{
    Task<Guid> CreateModule(ModuleData moduleData);
    Task DeleteModule(Guid guid);

    Task AddLessonIntoModule(Guid moduleGuid, Guid lessonGuid);
    
    Task DeleteLessonFromModule(Guid moduleGuid, Guid lessonGuid);
    Task<Module?> GetModule(Guid guid);
}