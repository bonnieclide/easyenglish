﻿using EasyEnglish.Service.CourseGeneratorService.Domain;

namespace EasyEnglish.Service.CourseGeneratorService.Services;

public interface IModuleRepository
{
    Task<Guid> AddModule(string name, string description, bool isFree);
    Task DeleteModule(Guid guid);
    Task<Module?> GetModule(Guid guid);
    Task AddLessonIntoModule(Guid moduleGuid, Guid lessonGuid);
    Task DeleteLessonFromModule(Guid moduleGuid, Guid lessonGuid);
}