﻿using EasyEnglish.Service.CourseGeneratorService.Domain;
using EasyEnglish.Service.CourseGeneratorService.Services.Dto;

namespace EasyEnglish.Service.CourseGeneratorService.Services;

public class LessonManager : ILessonManager
{
    private readonly ILessonRepository _lessonRepository;
    private readonly KafkaProducer _kafkaProducer = new ();

    public LessonManager(ILessonRepository lessonRepository)
    {
        _lessonRepository = lessonRepository;
    }

    public async Task<Guid> AddTheoryLesson(TheoryLessonData lessonData)
    {
        var theoryLesson = new TheoryLesson()
            .SetTitle(lessonData.Title)
            .SetText(lessonData.Text)
            .SetDescription(lessonData.Description)
            .SetIsFree(lessonData.IsFree);
        
        return await _lessonRepository.AddTheoryLesson(theoryLesson);
    }

    public async Task<Guid> AddQuizLesson(QuizLessonData lessonData)
    {
        var quizLesson = new QuizLesson()
            .SetTitle(lessonData.Title)
            .SetAnswer(lessonData.Answer)
            .SetQuestion(lessonData.Question)
            .SetDescription(lessonData.Description)
            .SetIsFree(lessonData.IsFree)
            .AddOptions(lessonData.Options);
        
        return await _lessonRepository.AddQuizLesson(quizLesson);
    }

    public async Task<Guid> AddAudioLesson(AudioLessonData lessonData)
    {
        var file = lessonData?.File;

        if (file is null)
            throw new Exception();
        
        byte[]? fileBytes;

        using (var ms = new MemoryStream())
        {
            await file.CopyToAsync(ms);
            fileBytes = ms?.ToArray();
        }

        if (fileBytes is null)
            throw new Exception();
        
        var fileGuid = await _kafkaProducer.SendFile(fileBytes, file.FileName);
        
        var audioLesson = new AudioLesson()
            .SetTitle(lessonData.Title)
            .SetDescription(lessonData.Description)
            .SetQuestion(lessonData.Question)
            .SetAnswer(lessonData.Answer)
            .SetIsFree(lessonData.IsFree)
            .SetFileGuid(fileGuid);
        
        return await _lessonRepository.AddAudioLesson(audioLesson);
    }

    public async Task DeleteLesson(Guid guid, LessonType lessonType)
    {
        switch (lessonType)
        {
            case LessonType.Theory: await _lessonRepository.DeleteLesson<TheoryLesson>(guid);
                break;
            case LessonType.Quiz: await _lessonRepository.DeleteLesson<QuizLesson>(guid);
                break;
            case LessonType.Audio: await _lessonRepository.DeleteLesson<AudioLesson>(guid);
                break;
            default:
                throw new Exception();
        }
    }

    public async Task<ILesson> GetLesson(Guid guid, LessonType lessonType)
    {
        return lessonType switch
        {
            LessonType.Theory => await _lessonRepository.GetLesson<TheoryLesson>(guid),
            LessonType.Quiz => await _lessonRepository.GetLesson<QuizLesson>(guid),
            LessonType.Audio => await _lessonRepository.GetLesson<AudioLesson>(guid),
            _ => throw new Exception()
        };
    }

    public async Task<IEnumerable<ILesson>> GetAllLessons(LessonType lessonType)
    {
        return lessonType switch
        {
            LessonType.Theory => await _lessonRepository.GetAllLessons<TheoryLesson>(),
            LessonType.Quiz => await _lessonRepository.GetAllLessons<QuizLesson>(),
            LessonType.Audio => await _lessonRepository.GetAllLessons<AudioLesson>(),
            _ => throw new Exception()
        };
    }
}