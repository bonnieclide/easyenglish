﻿using EasyEnglish.Service.CourseGeneratorService.Domain;
using EasyEnglish.Service.CourseGeneratorService.Services.Dto;

namespace EasyEnglish.Service.CourseGeneratorService.Services;

public interface ILessonManager
{
    Task<Guid> AddTheoryLesson(TheoryLessonData lessonData);
    Task<Guid> AddQuizLesson(QuizLessonData lessonData);
    Task<Guid> AddAudioLesson(AudioLessonData lessonData);
    Task DeleteLesson(Guid guid, LessonType lessonType);
    Task<ILesson> GetLesson(Guid guid, LessonType lessonType);
    Task<IEnumerable<ILesson>> GetAllLessons(LessonType lessonType);
}