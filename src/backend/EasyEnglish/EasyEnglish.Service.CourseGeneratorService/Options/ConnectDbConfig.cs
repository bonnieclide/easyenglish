﻿namespace EasyEnglish.Service.CourseGeneratorService.Options;

public class ConnectDbConfig
{
    public string? PostgreConnectionString { get; set; }
}