﻿using EasyEnglish.Service.CourseGeneratorService.Domain;
using EasyEnglish.Service.CourseGeneratorService.Services;
using EasyEnglish.Service.CourseGeneratorService.Services.Dto;
using Microsoft.AspNetCore.Mvc;

namespace EasyEnglish.Service.CourseGeneratorService.Controllers;

[ApiController]
[Route("[controller]")]
public class LessonController : Controller
{
    private readonly ILogger<LessonController> _logger;
    private readonly ILessonManager _lessonManager;

    public LessonController(ILogger<LessonController> logger, ILessonManager lessonManager)
    {
        _logger = logger;
        _lessonManager = lessonManager;
    }
    
    [HttpPost]
    [Route(nameof(AddTheoryLesson))]
    public async Task<ActionResult<long>> AddTheoryLesson(TheoryLessonData lessonData)
    {
        var id = await _lessonManager.AddTheoryLesson(lessonData);
        _logger.LogInformation("Added theory lesson with id: {0}", id);
        return Ok(id);
    }
    
    [HttpPost]
    [Route(nameof(AddQuizLesson))]
    public async Task<ActionResult<long>> AddQuizLesson(QuizLessonData lessonData)
    {
        var id = await _lessonManager.AddQuizLesson(lessonData);
        _logger.LogInformation("Added quiz lesson with id: {0}", id);
        return Ok(id);
    }
    
    [HttpPost]
    [Route(nameof(AddAudioLesson))]
    public async Task<ActionResult<long>> AddAudioLesson([FromForm]AudioLessonData lessonData)
    {
        var id = await _lessonManager.AddAudioLesson(lessonData);
        _logger.LogInformation("Added audio lesson with id: {0}", id);
        return Ok(id);
    }
    
    [HttpGet]
    [Route(nameof(GetLesson))]
    public async Task<ActionResult> GetLesson(Guid guid, string lessonType = "")
    {
        Enum.TryParse<LessonType>(lessonType, out var lessonTypeEnum);
        
        var lesson = await _lessonManager.GetLesson(guid, lessonTypeEnum);
        _logger.LogInformation("Lessons with guid: {0} was received!", guid);
        return Ok(lesson);
    }
    
    [HttpGet]
    [Route(nameof(GetAllLessons))]
    public async Task<ActionResult> GetAllLessons(string lessonType = "")
    {
        Enum.TryParse<LessonType>(lessonType, out var lessonTypeEnum);
        
        var lessons = await _lessonManager.GetAllLessons(lessonTypeEnum);
        _logger.LogInformation("Lessons with type: {0} was received!", lessonTypeEnum);
        return Ok(lessons);
    }
    
    [HttpDelete]
    [Route(nameof(DeleteLesson))]
    public async Task<ActionResult> DeleteLesson(Guid guid, string lessonType = "")
    {
        Enum.TryParse<LessonType>(lessonType, out var lessonTypeEnum);
        
        await _lessonManager.DeleteLesson(guid, lessonTypeEnum);
        _logger.LogInformation("Lesson with guid: {0} was delete!", guid);
        return Ok();
    }
}