using EasyEnglish.Service.CourseGeneratorService.Domain;
using EasyEnglish.Service.CourseGeneratorService.Services;
using EasyEnglish.Service.CourseGeneratorService.Services.Dto;
using Microsoft.AspNetCore.Mvc;

namespace EasyEnglish.Service.CourseGeneratorService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CourseController : Controller
    {
        private readonly ILogger<CourseController> _logger;
        private readonly ICourseManager _courseManager;

        public CourseController(ILogger<CourseController> logger, ICourseManager courseManager)
        {
            _logger = logger;
            _courseManager = courseManager;
        }

        [HttpPost]
        [Route(nameof(CreateCourse))]
        public async Task<ActionResult<Guid>> CreateCourse(CourseData courseData)
        {
            var guid = await _courseManager.CreateCourse(courseData);
            _logger.LogInformation("Created course with guid: {0}", guid);
            return Ok(guid);
        }
        
        [HttpDelete]
        [Route(nameof(DeleteCourse))]
        public async Task<ActionResult> DeleteCourse(Guid guid)
        {
            await _courseManager.DeleteCourse(guid);
            _logger.LogInformation("Course with guid: {0} was delete!", guid);
            return Ok();
        }

        [HttpGet]
        [Route(nameof(GetCourse))]
        public async Task<ActionResult<Course>> GetCourse(Guid guid)
        {
            var cource = await _courseManager.GetCourse(guid);
        
            if (cource is null)
                return BadRequest();
        
            _logger.LogInformation("Find course with guid: {0}", guid);
            return Ok(cource);
        }

        [HttpGet]
        [Route(nameof(GetAllCourses))]
        public async Task<ActionResult<Course[]>> GetAllCourses()
        {
            var courses = await _courseManager.GetAllCourses();
        
            _logger.LogInformation("Find all courses!");
            return Ok(courses);
        }

        [HttpGet]
        [Route(nameof(AddModuleInCourse))]
        public async Task<ActionResult> AddModuleInCourse(Guid courseGuid, Guid moduleGuid)
        {
            await _courseManager.AddModuleInCourse(courseGuid, moduleGuid);
            
            _logger.LogInformation("Module with guid {0} added into course with guid {1}", moduleGuid, courseGuid);
            return Ok();
        }
        
        [HttpDelete]
        [Route(nameof(DeleteModuleFromCourse))]
        public async Task<ActionResult> DeleteModuleFromCourse(Guid courseGuid, Guid moduleGuid)
        {
            await _courseManager.DeleteModuleFromCourse(courseGuid, moduleGuid);
            
            _logger.LogInformation("Module with guid {0} deleted from course with guid {1}", moduleGuid, courseGuid);
            return Ok();
        }
    }
}