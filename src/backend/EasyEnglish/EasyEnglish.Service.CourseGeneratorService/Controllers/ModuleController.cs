﻿using EasyEnglish.Service.CourseGeneratorService.Domain;
using EasyEnglish.Service.CourseGeneratorService.Services;
using EasyEnglish.Service.CourseGeneratorService.Services.Dto;
using Microsoft.AspNetCore.Mvc;

namespace EasyEnglish.Service.CourseGeneratorService.Controllers;

[ApiController]
[Route("[controller]")]
public class ModuleController : Controller
{
    private readonly ILogger<ModuleController> _logger;
    private readonly IModuleManager _moduleManager;

    public ModuleController(ILogger<ModuleController> logger, IModuleManager moduleManager)
    {
        _logger = logger;
        _moduleManager = moduleManager;
    }
    
    [HttpPost]
    [Route(nameof(CreateModule))]
    public async Task<ActionResult<Guid>> CreateModule(ModuleData moduleData)
    {
        var guid = await _moduleManager.CreateModule(moduleData);
        _logger.LogInformation("Created module with guid: {0}", guid);
        return Ok(guid);
    }
    
    [HttpDelete]
    [Route(nameof(DeleteModule))]
    public async Task<ActionResult> DeleteModule(Guid guid)
    {
        await _moduleManager.DeleteModule(guid);
        _logger.LogInformation("Module with guid: {0} was delete!", guid);
        return Ok();
    }
    
    [HttpGet]
    [Route(nameof(GetModule))]
    public async Task<ActionResult> GetModule(Guid guid)
    {
        var module = await _moduleManager.GetModule(guid);
        
        if (module is null)
            return BadRequest();
        
        _logger.LogInformation("Find module with id: {0}", module.Id);
        return Ok(module);
    }
    
    
    [HttpGet]
    [Route(nameof(AddLessonIntoModule))]
    public async Task<ActionResult> AddLessonIntoModule(Guid moduleGuid, Guid lessonGuid)
    {
        await _moduleManager.AddLessonIntoModule(moduleGuid, lessonGuid);
        _logger.LogInformation("Lesson with guid {0} added into module with guid {1}", lessonGuid, moduleGuid);
        return Ok();
    }
    
    [HttpDelete]
    [Route(nameof(DeleteLessonFromModule))]
    public async Task<ActionResult> DeleteLessonFromModule(Guid moduleGuid, Guid lessonGuid)
    {
        await _moduleManager.DeleteLessonFromModule(moduleGuid, lessonGuid);
        _logger.LogInformation("Lesson with guid {0} deleted from module with guid {1}", lessonGuid, moduleGuid);
        return Ok();
    }
}