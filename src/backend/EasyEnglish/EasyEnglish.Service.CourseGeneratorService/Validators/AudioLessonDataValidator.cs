﻿using EasyEnglish.Service.CourseGeneratorService.Services.Dto;
using FluentValidation;

namespace EasyEnglish.Service.CourseGeneratorService.Validators;

public class AudioLessonDataValidator : AbstractValidator<AudioLessonData>
{
    public AudioLessonDataValidator()
    {
        RuleFor(x => x.Answer)
            .NotNull()
            .WithMessage("Answer is null")
            .NotEmpty()
            .WithMessage("Answer is empty");
        RuleFor(x => x.File)
            .NotNull()
            .WithMessage("File is null");
    }
}