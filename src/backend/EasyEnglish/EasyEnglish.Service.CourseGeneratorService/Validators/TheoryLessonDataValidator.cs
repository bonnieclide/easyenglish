﻿using EasyEnglish.Service.CourseGeneratorService.Services.Dto;
using FluentValidation;

namespace EasyEnglish.Service.CourseGeneratorService.Validators;

public class TheoryLessonDataValidator : AbstractValidator<TheoryLessonData>
{
    public TheoryLessonDataValidator()
    {
        RuleFor(x => x.Text)
            .NotNull()
            .WithMessage("Text is null")
            .NotEmpty()
            .WithMessage("Text is empty");
    }
}