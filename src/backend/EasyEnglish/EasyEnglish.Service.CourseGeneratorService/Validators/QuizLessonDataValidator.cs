﻿using EasyEnglish.Service.CourseGeneratorService.Services.Dto;
using FluentValidation;

namespace EasyEnglish.Service.CourseGeneratorService.Validators;

public class QuizLessonDataValidator : AbstractValidator<QuizLessonData>
{
    public QuizLessonDataValidator()
    {
        RuleFor(x => x.Options)
            .NotNull()
            .WithMessage("Options is null")
            .Must((data, options) => CheckOptions(options, data.Answer))
            .WithMessage("Options don't have right answer");
        RuleFor(x => x.Answer)
            .NotEmpty()
            .WithMessage("Message cannot be null or empty!");
    }

    private bool CheckOptions(string[] options, string answer)
    {
        return options.Contains(answer);
    }
}