﻿namespace EasyEnglish.Service.CourseGeneratorService.Domain;

public enum LessonType
{
    Unknown = 0,
    Theory = 1,
    Quiz = 2,
    Audio = 3
}