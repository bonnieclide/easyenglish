﻿namespace EasyEnglish.Service.CourseGeneratorService.Domain;

public interface ILesson
{
    long Id { get; }
    Guid Guid { get; }
    string LessonType { get; }
}