﻿namespace EasyEnglish.Service.CourseGeneratorService.Domain;

public sealed class TheoryLesson : Entity, ILesson
{
    public string Title { get; private set; }
    public string Text { get; private set; }
    public bool IsFree { get; private set; }
    public string Description { get; private set; }

    public string LessonType => "Theory";
    
    public TheoryLesson SetText(string text)
    {
        Text = text;
        return this;
    }
    
    public TheoryLesson SetIsFree(bool isFree)
    {
        IsFree = isFree;
        return this;
    }
    
    public TheoryLesson SetTitle(string title)
    {
        Title = title;
        return this;
    }
    
    public TheoryLesson SetDescription(string description)
    {
        Description = description;
        return this;
    }
}