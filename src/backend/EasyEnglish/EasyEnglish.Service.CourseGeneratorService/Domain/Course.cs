﻿namespace EasyEnglish.Service.CourseGeneratorService.Domain;

public class Course : Entity
{
    private readonly List<Module> _modules = new();

    public string Name { get; private set; }
    public CourseType CourseType { get; private set; }
    public CourseLevel CourseLevel { get; private set; }
    public bool IsFree { get; private set; }
    public string Description { get; private set; }
    
    public IReadOnlyCollection<Module> Modules => _modules;

    public void AddModule(Module module)
    {
        _modules.Add(module);
    }

    public Course SetName(string name)
    {
        Name = name;
        return this;
    }

    public Course SetCourseType(CourseType courseType)
    {
        CourseType = courseType;
        return this;
    }

    public Course SetCourseLevel(CourseLevel courseLevel)
    {
        CourseLevel = courseLevel;
        return this;
    }

    public Course SetIsFree(bool isFree)
    {
        IsFree = isFree;
        return this;
    }
    
    public Course SetDescription(string description)
    {
        Description = description;
        return this;
    }
}