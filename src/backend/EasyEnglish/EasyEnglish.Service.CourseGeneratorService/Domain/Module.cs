﻿namespace EasyEnglish.Service.CourseGeneratorService.Domain;

public class Module : Entity
{
    private readonly List<AudioLesson> _audioLessons = new();
    private readonly List<QuizLesson> _quizLessons = new();
    private readonly List<TheoryLesson> _theoryLessons = new();
    
    public string Name { get; private set; }
    public bool IsFree { get; private set; }
    public string Description { get; private set; }

    public IReadOnlyCollection<AudioLesson> AudioLessons => _audioLessons;
    public IReadOnlyCollection<QuizLesson> QuizLessons => _quizLessons;
    public IReadOnlyCollection<TheoryLesson> TheoryLessons => _theoryLessons;
    
    public Module SetIsFree(bool isFree)
    {
        IsFree = isFree;
        return this;
    }
    
    public void AddAudioLesson(AudioLesson lesson)
    {
        if(!_audioLessons.Any(l => l.Guid == lesson.Guid))
            _audioLessons.Add(lesson);
    }
    
    public void AddQuizLesson(QuizLesson lesson)
    {
        if(!_quizLessons.Any(l => l.Guid == lesson.Guid))
            _quizLessons.Add(lesson);
    }
    
    public void AddTheoryLesson(TheoryLesson lesson)
    {
       if(!_theoryLessons.Any(l => l.Guid == lesson.Guid))
           _theoryLessons.Add(lesson);
    }
    
    public Module SetName(string name)
    {
        Name = name;
        return this;
    }
    
    public Module SetDescription(string description)
    {
        Description = description;
        return this;
    }
}