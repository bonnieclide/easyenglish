﻿namespace EasyEnglish.Service.CourseGeneratorService.Domain;

public class CourseType
{
    public int Id { get; private set; }
    public string Name { get; set; } = "English";
}