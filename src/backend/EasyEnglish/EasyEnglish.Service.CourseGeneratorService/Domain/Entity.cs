﻿namespace EasyEnglish.Service.CourseGeneratorService.Domain
{
    public class Entity
    {
        public long Id { get; private set; }
        public Guid Guid { get; private set; } 
    }
}
