﻿namespace EasyEnglish.Service.CourseGeneratorService.Domain;

public class CourseLevel
{
    public int Id { get; set; }
    public string Name { get; set; }
}