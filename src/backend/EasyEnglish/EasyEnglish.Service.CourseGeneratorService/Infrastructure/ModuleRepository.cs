﻿using Dapper;
using EasyEnglish.Service.CourseGeneratorService.Domain;
using EasyEnglish.Service.CourseGeneratorService.Options;
using EasyEnglish.Service.CourseGeneratorService.Services;
using Microsoft.Extensions.Options;
using Npgsql;

namespace EasyEnglish.Service.CourseGeneratorService.Infrastructure;

public class ModuleRepository : IModuleRepository
{
    private const string InsertPattern =
        @"insert into module (name, description, isFree)
          values(@Name, @Description, @IsFree)
          returning guid";
    
    private const string DeletePattern = @"delete from module where Guid = @Guid";
    
    private const string GetPattern = @"select course.id, course.guid, course.name, course.description, course.isFree,
                                        level.id, level.name, 
                                        type.id, type.name
                                        from course
                                        inner join courseType as type on type.id = course.courseTypeId
                                        inner join courseLevel as level on level.id = course.courseLevelId
                                        where course.guid = @Guid
                                        limit 1";
    
    private readonly string? _connectionString;
    private readonly SemaphoreSlim _semaphore = new(15, 15);
    
    public ModuleRepository(IOptions<ConnectDbConfig> sensorsPoolConfig)
    {
        _connectionString = sensorsPoolConfig.Value.PostgreConnectionString;
    }
    
    public async Task<Guid> AddModule(string name, string description, bool isFree)
    {
        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                Name = name,
                Description = description,
                IsFree = isFree
            };

            var result = await connection.ExecuteScalarAsync(InsertPattern, param);

            var guid = new Guid(result?.ToString() ?? string.Empty);

            return guid;
        }
        finally
        {
            _semaphore.Release();
        }
    }

    public async Task DeleteModule(Guid guid)
    {
        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                Guid = guid,
            };

            await connection.ExecuteAsync(DeletePattern, param);
        }
        finally
        {
            _semaphore.Release();
        }
    }

    public async Task<Module?> GetModule(Guid guid)
    {
        var sqlPattern = @"select module.id, module.guid, module.description, module.isfree, module.name,
        t.id, t.guid, t.isfree, t.description, t.text, t.title,
        a.id, a.guid, a.isfree, a.description, a.fileguid, a.title, a.question, q.answer,
        q.id, q.guid, q.isfree, q.description, q.options, q.question, q.answer
            from module
        left join lesson_to_module as l on l.moduleguid = @Guid
        left join theorylesson as t on l.lessonguid = t.guid
        left join audiolesson as a on l.lessonguid = a.guid
        left join quizlesson as q on l.lessonguid = q.guid
        where module.Guid = @Guid;";
        
        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                Guid = guid,
            };
            
            var moduleDictionary = new Dictionary<long, Module>();
            var theoryLessons = new Dictionary<long, TheoryLesson>();
            var audioLessons = new Dictionary<long, AudioLesson>();
            var quizLessons = new Dictionary<long, QuizLesson>();

            await connection.QueryAsync<Module, TheoryLesson, AudioLesson, QuizLesson, Module>(
                    sqlPattern, (module, theoryLesson, audioLesson, quizLesson) =>
                    {
                        var targetModule = module;
                        
                        if (moduleDictionary.TryGetValue(module.Id, out var currentModule))
                        {
                            targetModule = currentModule;
                        }
                        else
                        {
                            moduleDictionary.Add(module.Id, module);
                        }
                        
                        if (theoryLesson is not null && !theoryLessons.ContainsKey(theoryLesson.Id))
                        {
                            theoryLessons.Add(theoryLesson.Id, theoryLesson);
                            targetModule.AddTheoryLesson(theoryLesson);
                        }

                        if (audioLesson is not null && !audioLessons.ContainsKey(audioLesson.Id))
                        {
                            audioLessons.Add(audioLesson.Id, audioLesson);
                            targetModule.AddAudioLesson(audioLesson);
                        }

                        if (quizLesson is not null && !quizLessons.ContainsKey(quizLesson.Id))
                        {
                            quizLessons.Add(quizLesson.Id, quizLesson);
                            targetModule.AddQuizLesson(quizLesson);
                        }

                        return module;
                    },
                    param,
                    splitOn: "id");

            return moduleDictionary.Values.FirstOrDefault();
        }
        finally
        {
            _semaphore.Release();
        }
    }

    public async Task AddLessonIntoModule(Guid moduleGuid, Guid lessonGuid)
    {
        var sqlPattern = @"insert into lesson_to_module (lessonguid, moduleguid)
          values(@Lessonguid, @Moduleguid)";
        
        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                LessonGuid = lessonGuid,
                ModuleGuid = moduleGuid
            };

            await connection.ExecuteScalarAsync(sqlPattern, param);
        }
        finally
        {
            _semaphore.Release();
        }
    }

    public async Task DeleteLessonFromModule(Guid moduleGuid, Guid lessonGuid)
    {
        var sqlPattern = @"delete from lesson_to_module where moduleguid = @ModuleGuid and lessonguid = @LessonGuid";
        
        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                ModuleGuid = moduleGuid,
                LessonGuid = lessonGuid
            };

            await connection.ExecuteScalarAsync(sqlPattern, param);
        }
        finally
        {
            _semaphore.Release();
        }
    }
}