﻿using Dapper;
using EasyEnglish.Service.CourseGeneratorService.Domain;
using EasyEnglish.Service.CourseGeneratorService.Options;
using EasyEnglish.Service.CourseGeneratorService.Services;
using Microsoft.Extensions.Options;
using Npgsql;

namespace EasyEnglish.Service.CourseGeneratorService.Infrastructure;

public class LessonRepository : ILessonRepository
{
    private readonly string? _connectionString;
    private readonly SemaphoreSlim _semaphore = new(15, 15);

    public LessonRepository(IOptions<ConnectDbConfig> sensorsPoolConfig)
    {
        _connectionString = sensorsPoolConfig.Value.PostgreConnectionString;
    }
    
    public async Task<Guid> AddTheoryLesson(TheoryLesson lesson)
    {
    var sqlPattern = @"insert into theorylesson (title, text, description, isfree, lessontype)
          values(@Title, @Text, @Description, @IsFree, @LessonType)
          returning guid";

        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                Title = lesson.Title,
                Text = lesson.Text,
                IsFree = lesson.IsFree,
                Description = lesson.Description,
                LessonType = lesson.LessonType
            };

            var result = await connection.ExecuteScalarAsync(sqlPattern, param);

            var guid = new Guid(result?.ToString() ?? string.Empty);

            return guid;
        }
        finally
        {
            _semaphore.Release();
        }
    }

    public async Task<Guid> AddQuizLesson(QuizLesson lesson)
    {
        var sqlPattern = @"insert into quizlesson (title, question, answer, description, isfree, options, lessontype)
          values(@Title, @Question, @Answer, @Description, @IsFree, @Options, @LessonType)
          returning guid";
        
        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                Title = lesson.Title,
                Question = lesson.Question,
                Answer = lesson.Answer,
                IsFree = lesson.IsFree,
                Description = lesson.Description,
                Options = lesson.Options,
                LessonType = lesson.LessonType
            };

            var result = await connection.ExecuteScalarAsync(sqlPattern, param);

            var guid = new Guid(result?.ToString() ?? string.Empty);

            return guid;
        }
        finally
        {
            _semaphore.Release();
        }
    }

    public async Task<Guid> AddAudioLesson(AudioLesson lesson)
    {
        var sqlPattern = @"insert into audiolesson (title, question, answer, description, isfree, fileguid, lessontype)
          values(@Title, @Question, @Answer, @Description, @IsFree, @FileGuid, @LessonType)
          returning guid";

        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                Title = lesson.Title,
                Question = lesson.Question,
                Answer = lesson.Answer,
                IsFree = lesson.IsFree,
                Description = lesson.Description,
                FileGuid = lesson.FileGuid,
                LessonType = lesson.LessonType
            };

            var result = await connection.ExecuteScalarAsync(sqlPattern, param);

            var guid = new Guid(result?.ToString() ?? string.Empty);

            return guid;
        }
        finally
        {
            _semaphore.Release();
        }
    }

    public async Task<T> GetLesson<T>(Guid guid)
    {
        var tableName = GetTableNameByType(typeof(T));
        
        var sqlPattern = $"select * from {tableName} where Guid = @Guid";

        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                Guid = guid,
            };

            return await connection.QueryFirstOrDefaultAsync<T>(sqlPattern, param);
        }
        finally
        {
            _semaphore.Release();
        }
    }

    public async Task<IEnumerable<T>> GetAllLessons<T>()
    {
        var tableName = GetTableNameByType(typeof(T));
        
        var sqlPattern = $"select * from {tableName} where Id = @Id";

        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            return await connection.QueryAsync<T>(sqlPattern);
        }
        finally
        {
            _semaphore.Release();
        }
    }

    public async Task DeleteLesson<T>(Guid guid)
    {
        var tableName = GetTableNameByType(typeof(T));
        
        var sqlPattern = $"delete from {tableName} where Guid = @Guid";
        
        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                Guid = guid,
            };

            await connection.ExecuteScalarAsync(sqlPattern, param);
        }
        finally
        {
            _semaphore.Release();
        }
    }

    private string GetTableNameByType(Type type)
    {
        if(typeof(TheoryLesson) == type) 
            return@"theoryLesson";
        
        if(typeof(QuizLesson) == type) 
            return "quizLesson";
        
        if(typeof(AudioLesson) == type) 
            return "audioLesson";
        
        //TODO: Добавить пользовательское исключение
        throw new Exception();
    }
}