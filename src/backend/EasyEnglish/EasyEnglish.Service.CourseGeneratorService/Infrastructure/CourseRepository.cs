﻿using Dapper;
using EasyEnglish.Service.CourseGeneratorService.Domain;
using EasyEnglish.Service.CourseGeneratorService.Options;
using EasyEnglish.Service.CourseGeneratorService.Services;
using Microsoft.Extensions.Options;
using Npgsql;

namespace EasyEnglish.Service.CourseGeneratorService.Infrastructure;

public class CourseRepository : ICourseRepository
{
    private const string InsertPattern =
        @"insert into course (name, description, isFree, courseTypeId, courseLevelId)
          values(@Name, @Description, @IsFree,
                 (select id from courseType where name = @CourseTypeName), 
                 (select id from courseLevel where name = @CourseLevelName))
                 returning guid";


    private readonly string? _connectionString;
    private readonly SemaphoreSlim _semaphore = new(15, 15);

    public CourseRepository(IOptions<ConnectDbConfig> sensorsPoolConfig)
    {
        _connectionString = sensorsPoolConfig.Value.PostgreConnectionString;
    }

    public async Task<Guid> AddCourse(Course course)
    {
        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                Name = course.Name,
                CourseTypeName = course.CourseType.Name,
                CourseLevelName = course.CourseLevel.Name,
                Description = course.Description,
                IsFree = course.IsFree
            };

            var result = await connection.ExecuteScalarAsync(InsertPattern, param);

            var guid = new Guid(result?.ToString() ?? string.Empty);

            return guid;
        }
        finally
        {
            _semaphore.Release();
        }
    }

    public async Task DeleteCourse(Guid guid)
    {
        var sqlPattern = @"delete from course where Guid = @Guid";

        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                Guid = guid,
            };

            await connection.ExecuteAsync(sqlPattern, param);
        }
        finally
        {
            _semaphore.Release();
        }
    }

    public async Task<Course?> GetCourse(Guid guid)
    {
        var sqlPattern = @"select course.id, course.guid, course.name, course.description, course.isfree,
       type.id, type.name,
       level.id, level.name,
       m.id, m.guid, m.description, m.isfree, m.name,
       t.id, t.guid, t.isfree, t.description, t.text, t.title,
       a.id, a.guid, a.isfree, a.description, a.fileguid, a.title, a.question, a.answer,
       q.id, q.guid, q.isfree, q.description, q.options, q.question, q.answer
from course
         left join courseType as type on course.coursetypeid = type.id
         left join courselevel as level on course.courselevelid = level.id
         left join module_to_course as l on l.courseguid = @Guid
         left join module as m on l.moduleguid = m.guid
         left join lesson_to_module as lm on lm.moduleguid = m.guid
         left join theorylesson as t on lm.lessonguid = t.guid
         left join audiolesson as a on lm.lessonguid = a.guid
         left join quizlesson as q on lm.lessonguid = q.guid
where course.Guid = @Guid order by m.id;
";
        
        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                Guid = guid,
            };

            var courseDictionary = new Dictionary<long, Course>();
            var moduleDictionary = new Dictionary<long, Module>();
            var addedDictionary = new Dictionary<long, Module>();

            await connection.QueryAsync<Course, CourseType, CourseLevel, Module, TheoryLesson, AudioLesson, QuizLesson, Course>(
                sqlPattern, (course, courseType, courseLevel, module, theoryLesson, audioLesson, quizLesson) =>
                {
                    var targetCourse = course;
                    var targetModule = module;

                    if (courseDictionary.TryGetValue(course.Id, out var currentCourse))
                    {
                        targetCourse = currentCourse;
                    }
                    else
                    {
                        courseDictionary.Add(targetCourse.Id, targetCourse);
                        targetCourse.SetCourseLevel(courseLevel);
                        targetCourse.SetCourseType(courseType);
                    }

                    if (module != null && moduleDictionary.TryGetValue(module.Id, out var currentModule))
                    {
                        targetModule = currentModule;
                    }
                    else
                    {
                        if (module != null)
                            moduleDictionary.Add(module.Id, module);
                    }

                    if (theoryLesson is not null)
                        targetModule.AddTheoryLesson(theoryLesson);

                    if (audioLesson is not null)
                        targetModule.AddAudioLesson(audioLesson);

                    if (quizLesson is not null)
                        targetModule.AddQuizLesson(quizLesson);

                    if (module is not null && !addedDictionary.ContainsKey(module.Id))
                    {
                        addedDictionary.Add(module.Id, module);
                        targetCourse.AddModule(module);
                    }

                    return course;
                },
                param,
                splitOn: "id");

            return courseDictionary.Values.FirstOrDefault();
        }
        finally
        {
            _semaphore.Release();
        }
    }

    public async Task<Course[]> GetAllCourses()
    {
        var sqlPattern = @"select course.id, course.guid, course.name, course.description, course.isfree,
       type.id, type.name,
       level.id, level.name,
       m.id, m.guid, m.description, m.isfree, m.name,
       t.id, t.guid, t.isfree, t.description, t.text, t.title,
       a.id, a.guid, a.isfree, a.description, a.fileguid, a.title, a.question, a.answer,
       q.id, q.guid, q.isfree, q.description, q.options, q.question, q.answer
       from course
         left join courseType as type on course.coursetypeid = type.id
         left join courselevel as level on course.courselevelid = level.id
         left join module_to_course as l on l.courseguid = l.courseguid
         left join module as m on l.moduleguid = l.moduleguid
         left join lesson_to_module as lm on lm.moduleguid = m.guid
         left join theorylesson as t on lm.lessonguid = t.guid
         left join audiolesson as a on lm.lessonguid = a.guid
         left join quizlesson as q on lm.lessonguid = q.guid
       order by course.id, m.id desc;";

        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var courseDictionary = new Dictionary<long, Course>();
            var moduleDictionary = new Dictionary<long, Module>();
            var addedDictionary = new Dictionary<long, Module>();

            await connection.QueryAsync<Course, CourseType, CourseLevel, Module, TheoryLesson, AudioLesson, QuizLesson, Course>(
                sqlPattern, (course, courseType, courseLevel, module, theoryLesson, audioLesson, quizLesson) =>
                {
                    var targetCourse = course;
                    var targetModule = module;

                    if (courseDictionary.TryGetValue(course.Id, out var currentCourse))
                    {
                        targetCourse = currentCourse;
                    }
                    else
                    {
                        courseDictionary.Add(targetCourse.Id, targetCourse);
                        targetCourse.SetCourseLevel(courseLevel);
                        targetCourse.SetCourseType(courseType);
                    }

                    if (moduleDictionary.TryGetValue(module.Id, out var currentModule))
                        targetModule = currentModule;
                    else
                        moduleDictionary.Add(module.Id, module);

                    if (theoryLesson is not null)
                        targetModule.AddTheoryLesson(theoryLesson);

                    if (audioLesson is not null)
                        targetModule.AddAudioLesson(audioLesson);

                    if (quizLesson is not null)
                        targetModule.AddQuizLesson(quizLesson);

                    if (!addedDictionary.ContainsKey(module.Id))
                    {
                        addedDictionary.Add(module.Id, module);
                        targetCourse.AddModule(module);
                    }

                    return course;
                },
                splitOn: "id");

            return courseDictionary.Values.ToArray();
        }
        finally
        {
            _semaphore.Release();
        }
    }

    public async Task AddModule(Guid courseGuid, Guid moduleGuid)
    {
        var sqlPattern = @"insert into module_to_course (moduleguid, courseguid)
          values(@ModuleGuid, @CourseGuid)";

        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                ModuleGuid = moduleGuid,
                CourseGuid = courseGuid
            };

            await connection.ExecuteScalarAsync(sqlPattern, param);
        }
        finally
        {
            _semaphore.Release();
        }
    }

    public async Task DeleteModule(Guid courseGuid, Guid moduleGuid)
    {
        var sqlPattern = @"delete from module_to_course where moduleguid = @ModuleGuid and courseguid = @CourseGuid";

        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                ModuleGuid = moduleGuid,
                CourseGuid = courseGuid
            };

            await connection.ExecuteScalarAsync(sqlPattern, param);
        }
        finally
        {
            _semaphore.Release();
        }
    }
}