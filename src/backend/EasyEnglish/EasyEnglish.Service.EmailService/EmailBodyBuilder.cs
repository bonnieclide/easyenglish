﻿using System.Text;

namespace EasyEnglish.Service.EmailService;

public class EmailBodyBuilder
{
    public string BuildBody(string userName)
    {
        var stringBuilder = new StringBuilder();

        stringBuilder.AppendLine("<h3>");
        stringBuilder.AppendLine($"Congrats, {userName}!");
        stringBuilder.AppendLine("</h3>");
        stringBuilder.AppendLine(Environment.NewLine);

        stringBuilder.AppendLine("<p>");
        stringBuilder.AppendLine("You have been registered in the EasyEnglish portal!");
        stringBuilder.AppendLine("</p>");
        stringBuilder.AppendLine(Environment.NewLine);

        stringBuilder.AppendLine(Environment.NewLine);
        stringBuilder.AppendLine("<h3>");
        stringBuilder.AppendLine("Thanks!");
        stringBuilder.AppendLine("</h3>");

        return stringBuilder.ToString();
    }
}