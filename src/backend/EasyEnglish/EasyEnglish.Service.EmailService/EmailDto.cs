﻿namespace EasyEnglish.Service.EmailService;

public class EmailDto
{
    public string? Email { get; set; }
    public string? Name { get; set; }
}