﻿using System.Net.Mail;

namespace EasyEnglish.Service.EmailService;

public class EmailSender : IEmailSender
{
    private readonly ILogger<EmailSender> _logger;

    public EmailSender(ILogger<EmailSender> logger)
    {
        _logger = logger;
    }

    public Task<bool> TrySend(IReadOnlyCollection<SmtpClientData> smtpClientData)
    {
        try
        {
            Parallel.ForEach(smtpClientData, SendEmail);
            return Task.FromResult(true);
        }
        catch (Exception e)
        {
            _logger.LogWarning(e.Message);
            return Task.FromResult(false);
        }
    }

    private void SendEmail(SmtpClientData smtpClientData)
    {
        var smtpClient = new SmtpClient(smtpClientData.Host, smtpClientData.Port);

        smtpClient.Credentials = new System.Net.NetworkCredential(smtpClientData.UserName, smtpClientData.Password);

        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
        smtpClient.EnableSsl = true;
            
        var mail = new MailMessage();

        mail.From = new MailAddress(smtpClientData.Address, "Easy English");
        mail.To.Add(new MailAddress(smtpClientData.Email));
        mail.Body = smtpClientData.Body;
        mail.IsBodyHtml= true;
        mail.Subject = "Easy English registration";

        smtpClient.Send(mail);
    }
}