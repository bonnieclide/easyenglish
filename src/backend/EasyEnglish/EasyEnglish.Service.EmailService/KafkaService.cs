﻿using System.Diagnostics;
using Confluent.Kafka;
using EasyEnglish.Platform.Brokers;

namespace EasyEnglish.Service.EmailService;

public class KafkaService : BackgroundService
{
    private const string Topic = "easyenglish-registration-email";
    
    private readonly ILogger<KafkaService> _logger;
    private readonly IEmailSender _emailSender;
    private readonly int _batchCount = 100;
    private readonly Stopwatch _stopwatch = new();

    public KafkaService(ILogger<KafkaService> logger, IEmailSender emailSender)
    {
        _logger = logger;
        _emailSender = emailSender;
    }

    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        Task.Run(() => Watcher(stoppingToken));
        return Task.CompletedTask;
    }
    
    private async Task Watcher(CancellationToken cancellationToken)
    {
        using var consumer = new KafkaConsumerBuilder<Guid, EmailDto>()
            .SetGroupId("email-service")
            .Build();
        {
            consumer.Subscribe(Topic);
            
            _stopwatch.Start();

            var bodyBuilder = new EmailBodyBuilder();

            var emailBatch = new List<SmtpClientData>(_batchCount);
            var consumeResults = new List<ConsumeResult<Guid,EmailDto>?>(_batchCount);
            
            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    var consumeResult = consumer.Consume(cancellationToken);

                    var email = new SmtpClientData(
                        bodyBuilder.BuildBody(consumeResult.Message.Value.Name),
                        consumeResult.Message.Value.Email);

                    emailBatch.Add(email);
                    consumeResults.Add(consumeResult);

                    if (emailBatch.Count >= _batchCount || _stopwatch.Elapsed.Seconds > 1)
                    {
                        var sendResult = await _emailSender.TrySend(emailBatch);

                        if (!sendResult)
                            continue;

                        foreach (var result in consumeResults)
                            consumer.StoreOffset(result);

                        _logger.LogInformation("TrySend messages: {0}", emailBatch.Count);

                        emailBatch.Clear();
                        consumeResults.Clear();
                        _stopwatch.Restart();
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError("Error consume from topic: {0}. Error message: {1}", Topic, e.Message);
                }
            }

            consumer.Close();
        }
    }
}