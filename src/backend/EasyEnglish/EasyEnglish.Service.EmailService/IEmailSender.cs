﻿namespace EasyEnglish.Service.EmailService;

public interface IEmailSender
{
    Task<bool> TrySend(IReadOnlyCollection<SmtpClientData> smtpClientData); 
}