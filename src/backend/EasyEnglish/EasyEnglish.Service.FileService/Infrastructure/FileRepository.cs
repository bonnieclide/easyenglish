﻿using Dapper;
using EasyEnglish.Service.FileService.Domain;
using EasyEnglish.Service.FileService.Options;
using EasyEnglish.Service.FileService.Services;
using Microsoft.Extensions.Options;
using Npgsql;

namespace EasyEnglish.Service.FileService.Infrastructure;

public class FileRepository : IFileRepository
{
    private const string InsertPattern =
        @"insert into files (Guid, Data, ContentType, Filename) values(@Guid, @Data, @ContentType, @Filename)";

    private const string DeletePattern =
        @"delete from files where Guid = @Guid";

    private const string GetPattern =
        @"select * from files where Guid = @Guid";

    private readonly string? _connectionString;
    private readonly SemaphoreSlim _semaphore = new(15, 15);

    public FileRepository(IOptions<ConnectDbConfig> sensorsPoolConfig)
    {
        _connectionString = sensorsPoolConfig.Value.PostgreConnectionString;
    }

    public async Task AddFile(MediaFile file)
    {
        try
        {
            _semaphore.Wait();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                Guid = file.Guid,
                Data = file.Data,
                ContentType = file.ContentType,
                Filename = file.FileName
            };

            await connection.ExecuteAsync(InsertPattern, param);
        }
        finally
        {
            _semaphore.Release();
        }
    }

    public async Task DeleteFile(Guid guid)
    {
        try
        {
            _semaphore.Wait();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                Guid = guid.ToString(),
            };

            await connection.ExecuteAsync(DeletePattern, param);
        }
        finally
        {
            _semaphore.Release();
        }
    }

    public async Task<MediaFile?> GetFile(Guid guid)
    {
        try
        {
            _semaphore.Wait();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                Guid = guid.ToString(),
            };

            var queryResult = await connection.QueryAsync<MediaFile>(GetPattern, param);

            return queryResult?.SingleOrDefault();
        }
        finally
        {
            _semaphore.Release();
        }
    }
}