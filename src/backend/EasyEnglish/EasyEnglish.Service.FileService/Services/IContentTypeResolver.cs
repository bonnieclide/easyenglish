﻿namespace EasyEnglish.Service.FileService.Services;

public interface IContentTypeResolver
{
    string GetContentTypeByFileName(string fileName);
}