﻿using EasyEnglish.Service.FileService.Domain;

namespace EasyEnglish.Service.FileService.Services;

public interface IFileService
{
    Task AddFile(Guid guid, byte[] file, string contenType, string fileName);
    Task<MediaFile?> GetFile(Guid guid);
    Task DeleteFile(Guid guid);
}