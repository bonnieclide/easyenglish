﻿using EasyEnglish.Platform.Brokers;

namespace EasyEnglish.Service.FileService.Services;

public class KafkaService : BackgroundService
{
    private const string Topic = "easyenglish-add-file";
    
    private readonly ILogger<KafkaService> _logger;
    private readonly IFileService _fileService;
    private readonly IContentTypeResolver _contentTypeResolver;
    
    public KafkaService(ILogger<KafkaService> logger, 
        IFileService fileService, 
        IContentTypeResolver contentTypeResolver)
    {
        _logger = logger;
        _fileService = fileService;
        _contentTypeResolver = contentTypeResolver;
    }

    protected override Task ExecuteAsync(CancellationToken cancellationToken)
    {
        Task.Run(() => Watcher(cancellationToken), cancellationToken);
        return Task.CompletedTask;
    }

    private async void Watcher(CancellationToken cancellationToken)
    {
        using var consumer = new KafkaConsumerBuilder<Guid, FileDto>()
            .SetGroupId("easyenglish-file-service")
            .Build();
        {
            consumer.Subscribe(Topic);
            
            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    var consumeResult = consumer.Consume(cancellationToken);

                    var file = consumeResult.Message.Value;
                    
                    var contentType = _contentTypeResolver.GetContentTypeByFileName(file.Name);

                    await _fileService.AddFile(file.Guid, file.Data, contentType, file.Name);

                    consumer.StoreOffset(consumeResult);
                }
                catch (Exception e)
                {
                    _logger.LogError("Error consume from topic: {0}. Error message: {1}", Topic, e.Message);
                }
            }

            consumer.Close();
        }
    }
}
