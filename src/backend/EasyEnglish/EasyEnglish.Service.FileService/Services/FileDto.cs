﻿namespace EasyEnglish.Service.FileService.Services;

public class FileDto
{
    public Guid Guid { get; set; }
    public string Name { get; set; }
    public byte[] Data { get; set; }
}