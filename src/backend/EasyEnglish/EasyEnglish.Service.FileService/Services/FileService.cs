﻿using EasyEnglish.Service.FileService.Domain;

namespace EasyEnglish.Service.FileService.Services;

public class FileService : IFileService
{
    private readonly IFileRepository _fileRepository;
    private readonly ILogger<FileService> _logger;

    public FileService(IFileRepository fileRepository, ILogger<FileService> logger)
    {
        _fileRepository = fileRepository;
        _logger = logger;
    }

    public async Task AddFile(Guid guid, byte[] file, string contentType, string fileName)
    {
        await _fileRepository.AddFile(new MediaFile
        {
            Guid = guid.ToString(),
            Data = file,
            ContentType = contentType,
            FileName = fileName
        });
        
        _logger.LogInformation("The file with guid {0} has been added", guid);
    }

    public async Task<MediaFile?> GetFile(Guid guid)
    {
        return await _fileRepository.GetFile(guid);
    }

    public async Task DeleteFile(Guid guid)
    {
        await _fileRepository.DeleteFile(guid);
    }
}
