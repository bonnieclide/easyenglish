﻿using EasyEnglish.Service.FileService.Domain;

namespace EasyEnglish.Service.FileService.Services;

public interface IFileRepository
{
    Task AddFile(MediaFile file);
    Task DeleteFile(Guid guid);
    Task<MediaFile?> GetFile(Guid guid);
}