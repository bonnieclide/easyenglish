﻿using Microsoft.AspNetCore.StaticFiles;

namespace EasyEnglish.Service.FileService.Services;

public class ContentTypeResolver : IContentTypeResolver
{
    private readonly FileExtensionContentTypeProvider _typeProvider;

    public ContentTypeResolver()
    {
        _typeProvider = new FileExtensionContentTypeProvider();
    }

    public string GetContentTypeByFileName(string fileName)
    {
        _typeProvider.TryGetContentType(fileName, out var contentType);
        return contentType ?? "application/octet-stream";
    }
}