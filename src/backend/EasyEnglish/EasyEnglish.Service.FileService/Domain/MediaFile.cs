﻿namespace EasyEnglish.Service.FileService.Domain
{
    public class MediaFile
    {
        public int Id { get; set; }
        public string? Guid  { get; set; }
        public string? ContentType { get; set; }
        public byte[]? Data { get; set; }
        public string? FileName { get; set; }
    }
}
