﻿drop table files;

create table files
(
    id bigserial primary key,
    guid varchar(40) not null unique,
    data bytea not null,
    contentType text not null,
    fileName text not null
);

create index on files (guid);

select * from files;