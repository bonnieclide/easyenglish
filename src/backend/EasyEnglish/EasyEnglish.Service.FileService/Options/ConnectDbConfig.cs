﻿namespace EasyEnglish.Service.FileService.Options;

public class ConnectDbConfig
{
    public string? PostgreConnectionString { get; set; }
}