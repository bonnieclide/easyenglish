﻿using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Text.Json;

namespace EasyEnglish.Service.FileService.Middlewares
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionHandlingMiddleware> _logger;

        public ExceptionHandlingMiddleware(RequestDelegate next, ILogger<ExceptionHandlingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (ValidationException ex)
            {
                await HandleExceptionAsync(httpContext,
                    ex.Message,
                    HttpStatusCode.NotFound,
                    ex.Message);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext,
                    ex.Message,
                    HttpStatusCode.BadRequest,
                    "Request is bad!");
            }
        }

        private async Task HandleExceptionAsync(HttpContext httpContext,
            string errorMessage,
            HttpStatusCode httpStatusCode,
            string publicMessage)
        {
            _logger.LogWarning(errorMessage);

            var responce = httpContext.Response;
            responce.ContentType = "application/json";
            responce.StatusCode = (int)httpStatusCode;

            var result = JsonSerializer.Serialize(
                new
                {
                    Message = publicMessage,
                    StatusCode = (int)httpStatusCode
                });

            await responce.WriteAsync(result);
        }
    }
}
