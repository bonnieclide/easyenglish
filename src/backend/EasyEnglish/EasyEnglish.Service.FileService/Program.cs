using EasyEnglish.Platform.Brokers;
using EasyEnglish.Service.FileService.Infrastructure;
using EasyEnglish.Service.FileService.Middlewares;
using EasyEnglish.Service.FileService.Options;
using EasyEnglish.Service.FileService.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services.Configure<ConnectDbConfig>(builder.Configuration.GetSection("ConnectionStrings"));

builder.Services.AddSingleton<IFileRepository, FileRepository>();
builder.Services.AddSingleton<IFileService, FileService>();
builder.Services.AddSingleton<IContentTypeResolver, ContentTypeResolver>();

builder.Services.AddHostedService<KafkaService>();

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Host.UseCustomSerilog();

var app = builder.Build();

app.UseMiddleware<ExceptionHandlingMiddleware>();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
