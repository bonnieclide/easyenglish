﻿using EasyEnglish.Service.FileService.Services;
using Microsoft.AspNetCore.Mvc;

namespace EasyEnglish.Service.FileService.Controllers;

[ApiController]
[Route("[controller]")]
public class FileController : Controller
{
    private readonly ILogger<FileController> _logger;
    private readonly IFileService _fileService;

    public FileController(ILogger<FileController> logger, IFileService fileService)
    {
        _logger = logger;
        _fileService = fileService;
    }

    [HttpPost]
    [Route(nameof(AddFile))]
    [Consumes("multipart/form-data")]
    public async Task<ActionResult> AddFile([FromQuery]Guid guid, IFormFile formFile)
    {
        var file = HttpContext.Request.Form.Files.FirstOrDefault();

        if (file is null)
            return BadRequest();

        using (var ms = new MemoryStream())
        {
            file.CopyTo(ms);
            var fileBytes = ms.ToArray();

            await _fileService.AddFile(guid, fileBytes, file.ContentType, file.FileName);
        }

        _logger.LogInformation("Added file guid: {0} with type: {1}", guid, file.ContentType);

        return Ok();
    }

    [HttpGet]
    [Route(nameof(GetFile))]
    public async Task<ActionResult> GetFile(Guid guid)
    {
        var file = await _fileService.GetFile(guid);

        if (file is null)
        {
            _logger.LogWarning("File with guid: {0} not found", guid);
            return BadRequest();
        }

        _logger.LogInformation("Received file type {1} with guid: {0}", guid, file.ContentType);
        
        return new FileContentResult(file.Data, file.ContentType);
    }
 
    [HttpDelete]
    [Route(nameof(DeleteFile))]
    public async Task<ActionResult> DeleteFile(Guid guid)
    {
        await _fileService.DeleteFile(guid);
        _logger.LogInformation("File with guid: {0} was delete", guid);
        return Ok();
    }
}