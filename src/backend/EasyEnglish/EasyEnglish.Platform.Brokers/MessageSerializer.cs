﻿using System.Text.Json;
using Confluent.Kafka;

namespace EasyEnglish.Platform.Brokers;

public class MessageSerializer<T> : ISerializer<T>
{
    public byte[] Serialize(T data, SerializationContext context)
    {
        return JsonSerializer.SerializeToUtf8Bytes(data);
    }
}
