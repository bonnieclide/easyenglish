﻿using System.Text.Json;
using Confluent.Kafka;

namespace EasyEnglish.Platform.Brokers;

public class MessageDeserializer<T> : IDeserializer<T>
{
    public T Deserialize(ReadOnlySpan<byte> data, bool isNull, SerializationContext context)
    {
        return JsonSerializer.Deserialize<T>(data);
    }
}