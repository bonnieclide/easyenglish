﻿using Confluent.Kafka;

namespace EasyEnglish.Platform.Brokers;

public class KafkaConsumerBuilder<TKey, TValue> : IDisposable
{
    private IConsumer<TKey, TValue>? _consumer = null;
    
    private ConsumerConfig _consumerConfig = new ConsumerConfig
    {
        BootstrapServers = KafkaCommonParams.BootstrapServers,
        GroupId = Guid.NewGuid().ToString(),
        SaslUsername = KafkaCommonParams.SaslUsername,
        SaslPassword = KafkaCommonParams.SaslPassword,
        SecurityProtocol = KafkaCommonParams.SecurityProtocol,
        SaslMechanism = KafkaCommonParams.SaslMechanism,
        AutoOffsetReset = AutoOffsetReset.Latest,
        EnableAutoCommit = false,
        EnableAutoOffsetStore = false
    };

    public KafkaConsumerBuilder<TKey, TValue> SetGroupId(string groupId)
    {
        _consumerConfig.GroupId = groupId;
        return this;
    }

    public IConsumer<TKey, TValue> Build()
    {
        _consumer = new ConsumerBuilder<TKey, TValue>(_consumerConfig)
            .SetKeyDeserializer(new MessageDeserializer<TKey>())
            .SetValueDeserializer(new MessageDeserializer<TValue>())
            .Build();

        return _consumer;
    }

    public void Dispose()
    {
        _consumer?.Dispose();
    }
}