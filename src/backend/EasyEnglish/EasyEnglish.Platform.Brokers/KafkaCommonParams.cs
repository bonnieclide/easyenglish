﻿using Confluent.Kafka;

namespace EasyEnglish.Platform.Brokers;

public static class KafkaCommonParams
{
    public static readonly string BootstrapServers =
        "logical-giraffe-10203-eu1-kafka.upstash.io:9092";
    public static readonly string SaslUsername = "bG9naWNhbC1naXJhZmZlLTEwMjAzJNeYphYbAyxDqstov9-m6PP_FIAmcwwJILQ";
    public static readonly string SaslPassword = "IAuguITA7clBb-DycpFcZUaCFiZqGGQpEq4GqRAQWzMjVdWN5bBpzUns_HNDmgnMRkIgvw==";
    public static readonly SecurityProtocol SecurityProtocol = SecurityProtocol.SaslSsl;
    public static readonly SaslMechanism SaslMechanism = SaslMechanism.ScramSha256;
}