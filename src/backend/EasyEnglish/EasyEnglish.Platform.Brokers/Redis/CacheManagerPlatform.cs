﻿using System.Text.Json;
using StackExchange.Redis;

namespace EasyEnglish.Platform.Brokers.Redis;

public class CacheManagerPlatform
{
    private readonly ConnectionMultiplexer _redis = ConnectionMultiplexer.Connect(RedisCommonParams.ConnectionString);
    private readonly IDatabase _dataBase;

    public CacheManagerPlatform()
    {
        _dataBase = _redis.GetDatabase();;
    }

    public TimeSpan Ping()
    {
        return _dataBase.Ping();
    }

    public async Task<bool> AddAsync<T>(Guid guid, T value)
    {
        return await _dataBase.StringSetAsync(guid.ToString(), JsonSerializer.SerializeToUtf8Bytes(value));
    }
    
    public async Task<T?> GetAsync<T>(Guid guid)
    {
        var value = await _dataBase.StringGetAsync(guid.ToString());

        if (value.IsNullOrEmpty || !value.HasValue)
            return default;
        
        return JsonSerializer.Deserialize<T>(value);
    }
    
    // public Task<bool> DeleteAsync(Guid guid)
    // {
    //     return _dataBase.KeyDelete(guid.ToString());
    // }
}