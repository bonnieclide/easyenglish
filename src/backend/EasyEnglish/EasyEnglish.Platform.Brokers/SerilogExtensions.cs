﻿using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Graylog;

namespace EasyEnglish.Platform.Brokers;

public static  class SerilogExtensions
{
    public static IHostBuilder UseCustomSerilog(this IHostBuilder builder)
    {
        return builder.UseSerilog((ctx, lc) => lc
            .WriteTo.Console()
            .WriteTo.Graylog(new GraylogSinkOptions
            {
                HostnameOrAddress = "185.22.154.97",
                Port = 12201,
                MinimumLogEventLevel = LevelAlias.Minimum
            }));
    }
}