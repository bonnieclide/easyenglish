﻿using Confluent.Kafka;

namespace EasyEnglish.Platform.Brokers;

public class KafkaProducerBuilder<TKey, TValue> : IDisposable
{
    private IProducer<TKey, TValue> _producer;
    
    private ProducerConfig _producerConfig = new ProducerConfig
    {
        BootstrapServers = KafkaCommonParams.BootstrapServers,
        SaslUsername = KafkaCommonParams.SaslUsername,
        SaslPassword = KafkaCommonParams.SaslPassword,
        SecurityProtocol = KafkaCommonParams.SecurityProtocol,
        SaslMechanism = KafkaCommonParams.SaslMechanism,
    };
    
    public IProducer<TKey, TValue> Build()
    {
        _producer = new ProducerBuilder<TKey, TValue>(_producerConfig)
            .SetKeySerializer(new MessageSerializer<TKey>())
            .SetValueSerializer(new MessageSerializer<TValue>())
            .Build();

        return _producer;
    }

    public void Dispose()
    {
        _producer?.Dispose();
    }
}