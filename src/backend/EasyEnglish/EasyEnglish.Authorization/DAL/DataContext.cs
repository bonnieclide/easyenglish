﻿using EasyEnglish.Authorization.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace EasyEnglish.Authorization.DAL
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.
                Entity<User>()
                .HasIndex(u => u.Email)
                .IsUnique();
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql(b => b.MigrationsAssembly("EasyEnglish.Authorization"));

        public DbSet<User> Users => Set<User>();
        public DbSet<UserSession> UserSessions => Set<UserSession>();


    }
}
