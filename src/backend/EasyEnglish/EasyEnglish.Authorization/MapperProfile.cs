﻿using AutoMapper;
using EasyEnglish.Authorization.Helpers;

namespace EasyEnglish.Authorization
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Models.RegisterModel, DAL.Entities.User>()
                .ForMember(d => d.Id, m => m.MapFrom(s => Guid.NewGuid()))
                .ForMember(d => d.PasswordHash, m => m.MapFrom(s => HashHelper.GetHash(s.Password)));

            CreateMap<DAL.Entities.User, Models.UserModel>();
        }
    }
}
