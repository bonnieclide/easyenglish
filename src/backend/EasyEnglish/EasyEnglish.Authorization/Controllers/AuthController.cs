﻿using EasyEnglish.Authorization.DAL.Entities;
using EasyEnglish.Authorization.Models;
using EasyEnglish.Authorization.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

namespace EasyEnglish.Authorization.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IUsersService _usersService;
        private readonly IAuthService _authService;
        private readonly KafkaProducer _kafkaProducer;


        public AuthController(IUsersService usersService,
            IAuthService authService,
            KafkaProducer kafkaProducer)
        {
            _usersService = usersService;
            _authService = authService;
            _kafkaProducer = kafkaProducer;
        }

        [HttpPost]
        public async Task Register(RegisterModel model)
        {
            await _usersService.CreateUserAsync(model);
            await _kafkaProducer.SendEmail(model.Name, model.Email);
        }
           

        [HttpPost]
        public async Task<TokenModel> Login(LoginModel model)
            => await _authService.GetTokensAsync(model.Email, model.Password);

        [HttpPost]
        public async Task<TokenModel> RefreshToken(RefreshTokenRequestModel model)
            => await _authService.GetTokensByRefreshTokenAsync(model.RefreshToken);

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            var jwtString = HttpContext.Request.Headers.Authorization.ToString();
            await _authService.RemoveSession(jwtString);
            return Ok("You are logout");
        }





    }
}
