﻿using EasyEnglish.Authorization.Models;
using EasyEnglish.Authorization.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EasyEnglish.Authorization.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUsersService _usersService;

        public UserController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        [HttpGet]
        public async Task<UserModel> GetCurrentUser()
        {
            var jwtString = HttpContext.Request.Headers.Authorization.ToString();
            var user = await _usersService.GetCurrentUserByToken(jwtString);

            if (user == null)
            {
                throw new Exception("You are not authorized");
            }
            return user;
        }

        [HttpGet]
        public async Task<List<UserModel>> GetUsers()
        {
            return await _usersService.GetUsers();
        }



    }
}
