﻿using EasyEnglish.Authorization.Models;

namespace EasyEnglish.Authorization.Services
{
    public interface IUsersService
    {
        Task CreateUserAsync(RegisterModel model);
        Task<UserModel> GetUser(Guid id);
        Task<List<UserModel>> GetUsers();
        Task<UserModel> GetCurrentUserByToken(string token);
    }
}
