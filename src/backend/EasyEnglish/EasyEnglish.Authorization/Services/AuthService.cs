﻿using AutoMapper;
using EasyEnglish.Authorization.Configuration;
using EasyEnglish.Authorization.DAL;
using EasyEnglish.Authorization.DAL.Entities;
using EasyEnglish.Authorization.Helpers;
using EasyEnglish.Authorization.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace EasyEnglish.Authorization.Services
{
    public class AuthService : IAuthService
    {
        private readonly DataContext _context;
        private readonly AuthConfig _authConfig;

        public AuthService(DataContext context,
            IOptions<AuthConfig> config)
        {
            _context = context;
            _authConfig = config.Value;
        }



        public async Task<TokenModel> GetTokensAsync(string login, string password)
        {
            var user = await GetUserByCredentionAsync(login, password);
            var session = await _context.UserSessions.AddAsync(new DAL.Entities.UserSession
            {
                Id = Guid.NewGuid(),
                UserId = user.Id,
                Created = DateTime.UtcNow,
                RefreshToken = Guid.NewGuid()
            });
            await _context.SaveChangesAsync();
            return GenerateTokens(session.Entity);
        }

        public async Task<TokenModel> GetTokensByRefreshTokenAsync(string refreshToken)
        {
            var validParams = new TokenValidationParameters
            {
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateIssuerSigningKey = true,
                ValidateLifetime = true,
                IssuerSigningKey = _authConfig.SimmetricSecurityKey()
            };

            var principal = new JwtSecurityTokenHandler().ValidateToken(refreshToken, validParams, out var securityToken);

            if (securityToken is not JwtSecurityToken jwtToken
                || !jwtToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
            {
                throw new SecurityTokenException("Invalid token");
            }

            if (principal.Claims.FirstOrDefault(c => c.Type == "refreshToken")?.Value is String refreshIdString
                && Guid.TryParse(refreshIdString, out var refreshId))
            {
                var session = await GetSessionByRefreshToken(refreshId);
                if (!session.IsActive)
                {
                    throw new Exception("session is not active");
                }


                session.RefreshToken = Guid.NewGuid();
                await _context.SaveChangesAsync();

                return GenerateTokens(session);
            }
            else
            {
                throw new SecurityTokenException("Invalid token");
            }
        }

        public async Task<UserSession> GetSessionById(Guid id)
        {
            var session = await _context.UserSessions.FirstOrDefaultAsync(s => s.Id == id);
            if (session == null)
            {
                throw new Exception("session is not found");
            }
            return session;
        }

        public async Task RemoveSession(string jwtToken)
        {
            var sessionId = GetSessionIdByToken(jwtToken);

            var session = await GetSessionById(sessionId);
            _context.UserSessions.Remove(session);
            await _context.SaveChangesAsync();
        }

        private Guid GetSessionIdByToken(string jwtToken)
        {
            var validParams = new TokenValidationParameters
            {
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateIssuerSigningKey = true,
                ValidateLifetime = true,
                IssuerSigningKey = _authConfig.SimmetricSecurityKey()
            };

            var jwt = jwtToken.Split(' ');
            var principal = new JwtSecurityTokenHandler().ValidateToken(jwt[1], validParams, out var secTocken);
            var sessionIdString = principal.Claims.FirstOrDefault(c => c.Type == "sessionId")?.Value;
            Guid.TryParse(sessionIdString, out var sessionId);
            return sessionId ;
        }

        private TokenModel GenerateTokens(DAL.Entities.UserSession session)
        {
            var dtNow = DateTime.Now;
            var jwt = new JwtSecurityToken(
                issuer: _authConfig.Issuer,
                audience: _authConfig.Audience,
                notBefore: dtNow,
                claims: new Claim[]
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType,session.User.Name),
                    new Claim("Role",session.User.Role.ToString()),
                    new Claim("sessionId",session.Id.ToString()),
                    new Claim("userId",session.User.Id.ToString()),
                },
                expires: DateTime.Now.AddMinutes(_authConfig.LifeTime),
                signingCredentials: new SigningCredentials(_authConfig.SimmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var refresh = new JwtSecurityToken(
                notBefore: dtNow,
                claims: new Claim[]
                {
                    new Claim("refreshToken",session.RefreshToken.ToString()),
                },
                expires: DateTime.Now.AddHours(_authConfig.LifeTime),
                signingCredentials: new SigningCredentials(_authConfig.SimmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedRefresh = new JwtSecurityTokenHandler().WriteToken(refresh);

            return new TokenModel(encodedJwt, encodedRefresh);

        }

        private async Task<DAL.Entities.User> GetUserByCredentionAsync(string login, string password)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Email.ToLower() == login.ToLower());
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user),"User Not Found");
            }
            if (!HashHelper.Verify(password, user.PasswordHash))
            {
                throw new ArgumentException("Password Is Incorrect");
            }
            return user;

        }

        private async Task<UserSession> GetSessionByRefreshToken(Guid id)
        {
            var session = await _context.UserSessions.Include(x => x.User).FirstOrDefaultAsync(s => s.RefreshToken == id);
            if (session == null)
            {
                throw new Exception("session is not found");
            }
            return session;
        }

    }
}
