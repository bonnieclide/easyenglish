﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using EasyEnglish.Authorization.Configuration;
using EasyEnglish.Authorization.DAL;
using EasyEnglish.Authorization.DAL.Entities;
using EasyEnglish.Authorization.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

namespace EasyEnglish.Authorization.Services
{
    public class UsersService  : IUsersService
    {
        private readonly IMapper _mapper;
        private readonly DataContext _context;
        private readonly AuthConfig _authConfig;

        public UsersService(
            DataContext context,
            IOptions<AuthConfig> config,
            IMapper mapper )
        {
            _authConfig = config.Value;
            _context = context;
            _mapper = mapper;
        }


        public async Task CreateUserAsync(RegisterModel model)
        {
            var dbuser = _mapper.Map<DAL.Entities.User>(model);
            dbuser.Role = DAL.Enums.Role.User;
            await _context.Users.AddAsync(dbuser);
            await _context.SaveChangesAsync();
        }

        public async Task<Models.UserModel> GetUser(Guid id)
        {
            var user = await GetUserByIdAsync(id);
            return _mapper.Map<Models.UserModel>(user);
        }

        public async Task<Models.UserModel> GetCurrentUserByToken(string token)
        {
            var userId = GetUserIdByToken(token);
            var user = await GetUserByIdAsync(userId);
            return _mapper.Map<Models.UserModel>(user);
        }

        public async Task<List<Models.UserModel>> GetUsers()
        {
            return await _context.Users.AsNoTracking().ProjectTo<UserModel>(_mapper.ConfigurationProvider).ToListAsync();
        }

        private Guid GetUserIdByToken(string jwtToken)
        {
            var validParams = new TokenValidationParameters
            {
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateIssuerSigningKey = true,
                ValidateLifetime = true,
                IssuerSigningKey = _authConfig.SimmetricSecurityKey()
            };

            var jwt = jwtToken.Split(' ');
            var principal = new JwtSecurityTokenHandler().ValidateToken(jwt[1], validParams, out var secTocken);
            var userIdString = principal.Claims.FirstOrDefault(c => c.Type == "userId")?.Value;
            Guid.TryParse(userIdString, out var userId);
            return userId;
        }

        private async Task<DAL.Entities.User> GetUserByIdAsync(Guid id)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user),"User Not Found");
            }
            return user;
        }

    }
}
