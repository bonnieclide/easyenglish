﻿using EasyEnglish.Authorization.DAL.Entities;
using EasyEnglish.Authorization.Models;

namespace EasyEnglish.Authorization.Services
{
    public interface IAuthService
    {
        Task<TokenModel> GetTokensAsync(string login, string password);

        Task<TokenModel> GetTokensByRefreshTokenAsync(string refreshToken);

        Task<UserSession> GetSessionById(Guid id);

        Task RemoveSession(string jwt);

    }
}
