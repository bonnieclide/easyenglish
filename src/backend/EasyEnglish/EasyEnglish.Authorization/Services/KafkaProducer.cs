﻿using Confluent.Kafka;
using EasyEnglish.Platform.Brokers;

namespace EasyEnglish.Authorization.Services
{
    public class KafkaProducer 
    {
        readonly IProducer<Guid, object> _producer = new KafkaProducerBuilder<Guid, object>().Build();

        public async Task SendEmail(string name, string email)
        {
            await _producer.ProduceAsync("easyenglish-registration-email",
                new Message<Guid, object>
                {
                    Key = Guid.NewGuid(),
                    Value = new
                    {
                        Name = name,
                        Email = email
                    }
                });
        }

    }
}
