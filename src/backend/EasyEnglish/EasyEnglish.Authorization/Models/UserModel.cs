﻿using EasyEnglish.Authorization.DAL.Enums;

namespace EasyEnglish.Authorization.Models
{
    public class UserModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public Role Role { get; set; }

        public UserModel(Guid id, string name, string email, Role role)
        {
            Id = id;
            Name = name;
            Email = email;
            Role = role;
        }

    }
}
