﻿using System.ComponentModel.DataAnnotations;

namespace EasyEnglish.Authorization.Models
{
    public class RegisterModel 
    {
        [Required]
        public string Name { get; set; } = null!;
        [Required]
        public string Email { get; set; } = null!;
        [Required]
        public string Password { get; set; } = null!;
        [Required]
        [Compare(nameof(Password))]
        public string RetryPassword { get; set; } = null!;

    }
}
