﻿using EasyEnglish.Authorization.Middlewares;

namespace EasyEnglish.Authorization.Extensions
{
    public static class TokenValidateExtensions
    {
        public static IApplicationBuilder UseTokenValidation(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<TokenValidateMiddleware>();
        }
    }
}
