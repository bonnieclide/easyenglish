﻿using Confluent.Kafka;

namespace EasyEnglish.Platform;

public class ConsumerBuilder
{
    private ConsumerConfig GetConsumerConfig(string groupId)
    {
        return new ConsumerConfig
        {
            BootstrapServers = "glider-01.srvs.cloudkafka.com:9094,glider-02.srvs.cloudkafka.com:9094,glider-03.srvs.cloudkafka.com:9094",
            //GroupId = "add-files-group",
            GroupId = groupId,
            SaslUsername = "awxftq93",
            SaslPassword = "1vK34Wu7m5ri6qtCQZtYJRgzso_R7WMW",
            SecurityProtocol = SecurityProtocol.SaslSsl,
            SaslMechanism = SaslMechanism.ScramSha256,
            AutoOffsetReset = AutoOffsetReset.Earliest,
            EnableAutoCommit = false
        };
    }
}