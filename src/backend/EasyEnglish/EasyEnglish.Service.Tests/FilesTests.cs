using Confluent.Kafka;
using EasyEnglish.Platform.Brokers;

namespace EasyEnglish.Service.Tests;

public class FilesTests
{
    [Fact]
    public async void SendFile()
    {
        using var producer = new KafkaProducerBuilder<Guid, object>().Build();
        {
            byte[] bytes = await System.IO.File.ReadAllBytesAsync(
                @"files\I'll be back!.mp3");
            
            await producer.ProduceAsync("easyenglish-add-file",
                new Message<Guid, object>
                {
                    Key = Guid.NewGuid(),
                    Value = new
                    {
                        Guid = Guid.NewGuid(),
                        Name = "testFile.mp3",
                        Data = bytes
                    }
                });
        }
    }
}