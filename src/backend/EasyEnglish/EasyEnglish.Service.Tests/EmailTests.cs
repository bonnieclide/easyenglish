using Confluent.Kafka;
using EasyEnglish.Platform.Brokers;

namespace EasyEnglish.Service.Tests;

public class EmailTests
{
    [Fact]
    public void SendEmail()
    {
        using var producer = new KafkaProducerBuilder<Guid, object>().Build();
        {
            for (int i = 0; i < 10; i++)
            {
                producer.Produce("easyenglish-registration-email",
                    new Message<Guid, object>
                    {
                        Key = Guid.NewGuid(),
                        Value = new
                        {
                            Name = "Vasya",
                            Email = "web.andrej@gmail.com",
                        }
                    });
            }
            
            producer.Flush();
        }
    }
}