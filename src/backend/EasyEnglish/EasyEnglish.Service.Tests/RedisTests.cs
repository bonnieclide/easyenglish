﻿using EasyEnglish.Platform.Brokers.Redis;

namespace EasyEnglish.Service.Tests;

public class RedisTests
{
    [Fact]
    public async void Add_And_Get_Redis_Test()
    {
        var cacheManager = new CacheManagerPlatform();
        cacheManager.Ping();
        var guid = Guid.NewGuid();
        var testData = new List<string> { "test1", "test2" };
        await cacheManager.AddAsync(guid, testData);
        var result = await cacheManager.GetAsync<List<string>>(guid);

        if (result == null || !testData.SequenceEqual(result))
            throw new Exception();
    }
}