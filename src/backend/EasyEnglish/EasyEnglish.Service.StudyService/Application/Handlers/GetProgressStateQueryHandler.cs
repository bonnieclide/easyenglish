﻿using EasyEnglish.Service.StudyService.Application.Queries;
using EasyEnglish.Service.StudyService.Application.Services;
using EasyEnglish.Service.StudyService.ViewModels;
using MediatR;

namespace EasyEnglish.Service.StudyService.Application.Handlers;

public class GetProgressStateQueryHandler : IRequestHandler<ProgressStateQuery, ProgressView>
{
    private readonly IStudyUnitRepository _studyUnitRepository;
    private readonly ICourseLoaderService _courseLoader;
    private readonly ILogger<GetProgressStateQueryHandler> _logger;

    public GetProgressStateQueryHandler(IStudyUnitRepository studyUnitRepository, 
        ICourseLoaderService courseLoader, 
        ILogger<GetProgressStateQueryHandler> logger)
    {
        _studyUnitRepository = studyUnitRepository;
        _courseLoader = courseLoader;
        _logger = logger;
    }

    public async Task<ProgressView> Handle(ProgressStateQuery request, CancellationToken cancellationToken)
    {
        if (!request.CourseGuid.HasValue || !request.UserGuid.HasValue)
            return ProgressView.DefaultProgressView();
        
        var studyUnits = await _studyUnitRepository.GetStudyUnit(request.UserGuid.Value, request.CourseGuid.Value);
        var course = await _courseLoader.LoadCourse(request.CourseGuid.Value);

        if (!studyUnits.Any() || course == null)
        {
            _logger.LogWarning("Progress for user {UserGuid} is error", request.UserGuid.Value);
            return ProgressView.DefaultProgressView();
        }
        
        _logger.LogInformation("Get progress for user {UserGuid}", request.UserGuid.Value);

        return ProgressView.GetProgressView(studyUnits, course);
    }
}