﻿using EasyEnglish.Service.StudyService.Application.Commands;
using EasyEnglish.Service.StudyService.Application.Factories;
using EasyEnglish.Service.StudyService.Application.Services;
using EasyEnglish.Service.StudyService.ViewModels;
using MediatR;

namespace EasyEnglish.Service.StudyService.Application.Handlers;

public class SetAnswerCommandHandler : IRequestHandler<SetAnswerCommand, SetAnswerResultView>
{
    private readonly IAnswerChecker _checker;
    private readonly ICourseLoaderService _courseLoader;
    private readonly IStudyUnitRepository _studyUnitRepository;
    private readonly ILogger<SetAnswerCommandHandler> _logger;

    public SetAnswerCommandHandler(IAnswerChecker checker, 
        ICourseLoaderService courseLoader, 
        IStudyUnitRepository studyUnitRepository, 
        ILogger<SetAnswerCommandHandler> logger)
    {
        _checker = checker;
        _courseLoader = courseLoader;
        _studyUnitRepository = studyUnitRepository;
        _logger = logger;
    }

    public async Task<SetAnswerResultView> Handle(SetAnswerCommand request, CancellationToken cancellationToken)
    {
        var lesson = await _courseLoader.LoadLesson(request.LessonGuid, request.LessonType);

        if (lesson is null)
        {
            _logger.LogWarning("Lesson guid: {LessonGuid} with type {LessonType} not load!", 
                request.LessonGuid,
                request.LessonType);
            
            throw new Exception();
        }

        _logger.LogInformation("Start check answers. Answer: {LessonAnswer}, user answer: {UserAnswer}", 
            lesson?.Answer ?? "emptyAnswer", 
            request.Answer?? "emptyAnswer");

        var checkResult = _checker.Check(lesson, request.Answer);

        if (request.UserGuid != null && request.CourseGuid != null && request.LessonGuid != null)
        {
            var studyUnit = StudyUnitUnitFactory.Build(request.UserGuid.Value, request.CourseGuid.Value, request.LessonGuid.Value, checkResult);
            await _studyUnitRepository.AddStudyUnit(studyUnit);
        }
        
        _logger.LogInformation("End check answers. Answer: {LessonAnswer}, user answer: {UserAnswer}. Result {CheckResult}", 
            lesson?.Answer ?? "emptyAnswer", 
            request.Answer?? "emptyAnswer",
            checkResult);

        return new SetAnswerResultView
        {
            Result = checkResult
        };
    }
}