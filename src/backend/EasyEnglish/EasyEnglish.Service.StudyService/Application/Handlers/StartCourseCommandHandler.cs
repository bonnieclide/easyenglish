﻿using EasyEnglish.Service.StudyService.Application.Commands;
using EasyEnglish.Service.StudyService.Application.Events;
using MediatR;

namespace EasyEnglish.Service.StudyService.Application.Handlers;

public class StartCourseCommandHandler : IRequestHandler<StartCourseCommand, bool>
{
    private readonly IMediator _mediator;
    private readonly ILogger<StartCourseCommandHandler> _logger;

    public StartCourseCommandHandler(IMediator mediator, ILogger<StartCourseCommandHandler> logger)
    {
        _mediator = mediator;
        _logger = logger;
    }

    public async Task<bool> Handle(StartCourseCommand request, CancellationToken cancellationToken)
    {
        try
        {
            await _mediator.Publish(new StartCourseEvent
            {
                UserGuid = request.UserGuid.Value,
                CourseGuid = request.CourseGuid.Value
            }, cancellationToken);
            
            _logger.LogInformation("User with {UserGuid} started lesson with guid {CourseGuid}",
                request.UserGuid.Value,
                request.CourseGuid.Value);

            return true;
        }
        catch (Exception e)
        {
            _logger.LogInformation("User with {UserGuid} not started lesson with guid {CourseGuid}. Error message : {Message}",
                request.UserGuid.Value,
                request.CourseGuid.Value,
                e.Message);
            
            return false;
        }
    }
}