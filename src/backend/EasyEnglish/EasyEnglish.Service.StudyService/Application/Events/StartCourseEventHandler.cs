﻿using EasyEnglish.Service.StudyService.Application.Services;
using MediatR;
using StackExchange.Redis;

namespace EasyEnglish.Service.StudyService.Application.Events;

public class StartCourseEventHandler : INotificationHandler<StartCourseEvent>
{
    private readonly ICourseLoaderService _courseLoaderService;
    private readonly IStartedCourseRepository _repository;

    public StartCourseEventHandler(ICourseLoaderService courseLoaderService, 
        IStartedCourseRepository repository)
    {
        _courseLoaderService = courseLoaderService;
        _repository = repository;
    }

    public async Task Handle(StartCourseEvent notification, CancellationToken cancellationToken)
    {
        //var course = await _courseLoaderService.LoadCourse(notification.CourseGuid);
        
        await _repository.AddCourse(notification.UserGuid, notification.CourseGuid);
    }
}