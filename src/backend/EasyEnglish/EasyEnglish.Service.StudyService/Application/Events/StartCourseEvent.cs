﻿using MediatR;

namespace EasyEnglish.Service.StudyService.Application.Events;

public class StartCourseEvent : INotification
{
    public Guid UserGuid { get; set; }
    public Guid CourseGuid { get; set; }
}