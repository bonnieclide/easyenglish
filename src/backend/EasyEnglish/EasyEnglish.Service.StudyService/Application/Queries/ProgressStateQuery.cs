﻿using EasyEnglish.Service.StudyService.ViewModels;
using MediatR;

namespace EasyEnglish.Service.StudyService.Application.Queries;

public class ProgressStateQuery : IRequest<ProgressView>
{
    public Guid? UserGuid { get; set; } 
    public Guid? CourseGuid { get; set; }
}