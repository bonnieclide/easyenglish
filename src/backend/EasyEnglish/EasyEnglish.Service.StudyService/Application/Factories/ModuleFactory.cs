﻿using EasyEnglish.Service.StudyService.Application.Dto;
using EasyEnglish.Service.StudyService.Domain;

namespace EasyEnglish.Service.StudyService.Application.Factories;

public class ModuleFactory
{
    public static Module Build(ModuleDto moduleDto)
    {
        return new Module()
            .SetGuid(moduleDto.Guid)
            .SetName(moduleDto.Name)
            .SetDescription(moduleDto.Description)
            .SetIsFree(moduleDto.IsFree)
            .AddLessons(moduleDto.AudioLessons?.Select(LessonFactory.Build)?.ToArray())
            .AddLessons(moduleDto.QuizLessons?.Select(LessonFactory.Build)?.ToArray())
            .AddLessons(moduleDto.TheoryLessons?.Select(LessonFactory.Build)?.ToArray());
    }
}