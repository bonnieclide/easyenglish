﻿using EasyEnglish.Service.StudyService.Application.Dto;
using EasyEnglish.Service.StudyService.Application.Services;
using EasyEnglish.Service.StudyService.Domain;

namespace EasyEnglish.Service.StudyService.Application.Factories;

public class CourseFactory
{
    public static Course Build(CourseDto courseDto)
    {
        return new Course()
            .SetGuid(courseDto.Guid)
            .SetName(courseDto.Name)
            .SetDescription(courseDto.Description)
            .SetIsFree(courseDto.IsFree)
            .SetCourseLevel(courseDto?.CourseLevel?.Name)
            .SetCourseType(courseDto?.CourseType?.Name)
            .AddModules(courseDto?.Modules?.Select(ModuleFactory.Build)?.ToArray());
    }
}