﻿using EasyEnglish.Service.StudyService.Domain;

namespace EasyEnglish.Service.StudyService.Application.Factories;

public class StudyUnitUnitFactory
{
    public static StudyUnit Build(Guid userGuid, Guid courseGuid, Guid lessonGuid, bool answerState)
    {
        return new StudyUnit()
            .SetUserGuid(userGuid)
            .SetCourseGuid(courseGuid)
            .SetLessonGuid(lessonGuid)
            .SetAnswerState(answerState);
    }
}