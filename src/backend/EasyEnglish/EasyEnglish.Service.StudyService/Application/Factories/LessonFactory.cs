﻿using EasyEnglish.Service.StudyService.Application.Dto;
using EasyEnglish.Service.StudyService.Domain;

namespace EasyEnglish.Service.StudyService.Application.Factories;

public class LessonFactory
{
    public static Lesson Build(LessonDto lessonDto)
    {
        return new Lesson()
            .SetGuid(lessonDto.Guid)
            .SetName(lessonDto.Name)
            .SetDescription(lessonDto.Description)
            .SetIsFree(lessonDto.IsFree)
            .SetLessonType(lessonDto.LessonType)
            .SetAnswer(lessonDto.Answer);
    }
}