﻿using System.Text.Json.Serialization;

namespace EasyEnglish.Service.StudyService.Application.Dto;

public class ModuleDto
{
    [JsonPropertyName("guid")]
    public Guid? Guid { get; set; }
    
    [JsonPropertyName("name")]
    public string? Name { get; set; }
    
    [JsonPropertyName("isFree")]
    public bool? IsFree { get; set; }
    
    [JsonPropertyName("description")]
    public string? Description { get; set; }
    
    [JsonPropertyName("audioLessons")]
    public IReadOnlyCollection<LessonDto> AudioLessons { get; set; }
    
    [JsonPropertyName("quizLessons")]
    public IReadOnlyCollection<LessonDto> QuizLessons { get; set; }
    
    [JsonPropertyName("theoryLessons")]
    public IReadOnlyCollection<LessonDto> TheoryLessons { get; set; }
}