﻿using System.Text.Json.Serialization;

namespace EasyEnglish.Service.StudyService.Application.Dto;

public class CourseTypeDto
{
    [JsonPropertyName("name")]
    public string? Name { get; set; }
}