﻿using System.Text.Json;
using EasyEnglish.Service.StudyService.Application.Dto;
using EasyEnglish.Service.StudyService.Application.Factories;
using EasyEnglish.Service.StudyService.Domain;
using EasyEnglish.Service.StudyService.Options;
using Microsoft.Extensions.Options;

namespace EasyEnglish.Service.StudyService.Application.Services;

public class CourseLoaderService : ICourseLoaderService
{
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly IOptions<EasyEnglishServicesOptions> _options;

    public CourseLoaderService(IHttpClientFactory httpClientFactory, 
        IOptions<EasyEnglishServicesOptions> options)
    {
        _httpClientFactory = httpClientFactory;
        _options = options;
    }

    public async Task<Course?> LoadCourse(Guid courseGuid)
    {
        var requestUri = $"http://{_options.Value.ServerAddress}/Course/GetCourse?guid={courseGuid}";
        
        var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, requestUri);
        
        var httpClient = _httpClientFactory.CreateClient();
        var httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);
        
        if (httpResponseMessage.IsSuccessStatusCode)
        {
            var contentStream =
                await httpResponseMessage.Content.ReadAsStringAsync();
            
            var courseDto = JsonSerializer.Deserialize<CourseDto>(contentStream);

            if (courseDto is null)
                return default;
            
            return CourseFactory.Build(courseDto);
        }

        return default;
    }

    public async Task<Module?> LoadModule(Guid? moduleGuid)
    {
        var requestUri = $"http://{_options.Value.ServerAddress}/Course/GetModule?guid={moduleGuid}";
        
        var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, requestUri);
        
        var httpClient = _httpClientFactory.CreateClient();
        var httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);
        
        if (httpResponseMessage.IsSuccessStatusCode)
        {
            var contentStream =
                await httpResponseMessage.Content.ReadAsStringAsync();
            
            var moduleDto = JsonSerializer.Deserialize<ModuleDto>(contentStream);

            if (moduleDto is null)
                return default;
            
            return ModuleFactory.Build(moduleDto);
        }

        return default;
    }

    public async Task<Lesson?> LoadLesson(Guid? lessonGuid, string? lessonType)
    {
        var requestUri = $"http://{_options.Value.ServerAddress}/Lesson/GetLesson?guid={lessonGuid}&lessonType={lessonType}";
        
        var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, requestUri);
        
        var httpClient = _httpClientFactory.CreateClient();
        var httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);
        
        if (httpResponseMessage.IsSuccessStatusCode)
        {
            var contentStream =
                await httpResponseMessage.Content.ReadAsStringAsync();
            
            var lessonDto = JsonSerializer.Deserialize<LessonDto>(contentStream);

            if (lessonDto is null)
                return default;
            
            return LessonFactory.Build(lessonDto);
        }

        return default;
    }
}