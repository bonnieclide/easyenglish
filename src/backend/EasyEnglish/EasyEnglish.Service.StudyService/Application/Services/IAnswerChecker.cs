﻿using EasyEnglish.Service.StudyService.Domain;

namespace EasyEnglish.Service.StudyService.Application.Services;

public interface IAnswerChecker
{
    bool Check(Lesson? lesson, string? answer);
}