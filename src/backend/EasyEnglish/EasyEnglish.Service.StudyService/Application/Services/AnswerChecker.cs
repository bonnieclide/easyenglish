﻿using EasyEnglish.Service.StudyService.Domain;

namespace EasyEnglish.Service.StudyService.Application.Services;

public class AnswerChecker : IAnswerChecker
{
    private const int TruePercent = 90;
    private readonly ILogger<AnswerChecker> _logger;

    public AnswerChecker(ILogger<AnswerChecker> logger)
    {
        _logger = logger;
    }

    public bool Check(Lesson? lesson, string? userAnswer)
    {
        if(lesson == null)
            return false;
        
        if (lesson.Answer == null)
            return true;

        if (userAnswer == null)
            return false;
        
        if (lesson.Answer == userAnswer)
            return true;

        var result = CheckAnswerByChars(lesson.Answer, userAnswer);

        return result;
    }

    private bool CheckAnswerByChars(string answer, string userAnswer)
    {
        var right = new List<bool>();
        
        var answerChars = answer.ToCharArray();
        var userAnswerChars = userAnswer.ToCharArray();

        var maxCount = answerChars.Length;

        if (maxCount <= 0)
            return false;
        
        for (var i = 0; i < maxCount; i++)
        {
            try
            {
                right.Add(answerChars[i] == userAnswerChars[i]);
            }
            catch
            {
                right.Add(false);
            }
        }
        
        var trueCount = right.Where(r => r).ToArray().Length;

        if (userAnswerChars.Length > answerChars.Length)
            trueCount -= userAnswerChars.Length - answerChars.Length;

        var truePercent = 100 - ((maxCount - trueCount) * 100 / maxCount);
        
        _logger.LogInformation("Percent: {TruePercent}", truePercent);

        if (truePercent >= TruePercent)
            return true;

        return false;
    }
}