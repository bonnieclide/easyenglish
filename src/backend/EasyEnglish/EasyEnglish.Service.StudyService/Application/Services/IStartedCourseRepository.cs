﻿namespace EasyEnglish.Service.StudyService.Application.Services;

public interface IStartedCourseRepository
{
    Task<bool> AddCourse(Guid userGuid, Guid courseGuid);
}