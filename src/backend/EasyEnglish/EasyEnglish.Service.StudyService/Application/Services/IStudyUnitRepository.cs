﻿using EasyEnglish.Service.StudyService.Domain;

namespace EasyEnglish.Service.StudyService.Application.Services;

public interface IStudyUnitRepository
{
    Task<bool> AddStudyUnit(StudyUnit studyUnit);

    Task<StudyUnit[]> GetStudyUnit(Guid userGuid, Guid courseGuid);
}