﻿using EasyEnglish.Service.StudyService.Domain;

namespace EasyEnglish.Service.StudyService.Application.Services;

public interface ICourseLoaderService
{
    public Task<Course?> LoadCourse(Guid courseGuid);
    public Task<Module?> LoadModule(Guid? moduleGuid);
    public Task<Lesson?> LoadLesson(Guid? lessonGuid, string? lessonType);
}