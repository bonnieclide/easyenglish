﻿using MediatR;

namespace EasyEnglish.Service.StudyService.Application.Commands;

public class StartCourseCommand : IRequest<bool>
{
    public Guid? UserGuid { get; set; } 
    public Guid? CourseGuid { get; set; }
}