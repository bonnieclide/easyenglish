﻿using EasyEnglish.Service.StudyService.ViewModels;
using MediatR;

namespace EasyEnglish.Service.StudyService.Application.Commands;

public class SetAnswerCommand : IRequest<SetAnswerResultView>
{
    public Guid? UserGuid { get; set; } 
    public Guid? CourseGuid { get; set; }
    public Guid? LessonGuid { get; set; }
    public string? LessonType { get; set; }
    public string? Answer { get; set; }
}