﻿using EasyEnglish.Service.StudyService.Domain;

namespace EasyEnglish.Service.StudyService.ViewModels;

public class ProgressView
{
    public IDictionary<Guid, bool> Results { get; } = new Dictionary<Guid, bool>();

    public static ProgressView DefaultProgressView()
    {
        return new ProgressView();
    }

    public void AddResult(Guid lessonGuid, bool state)
    {
        Results.TryAdd(lessonGuid, state);
    }
    
    public static ProgressView GetProgressView(StudyUnit[] units, Course course)
    {
        var result = new ProgressView();
        
        var results = units.GroupBy(u => u.LessonGuid)
            .ToDictionary(g => g.Key, g => g.MaxBy(u=>u.Timestamp));
        
        foreach (var module in course.Modules!)
        {
            foreach (var audio in module.AudioLessons)
            {
                if (results.TryGetValue(audio.Guid, out var studyUnit))
                    result.AddResult(audio.Guid, studyUnit!.AnswerState);
            }
            
            foreach (var quiz in module.QuizLessons)
            {
                if (results.TryGetValue(quiz.Guid, out var studyUnit))
                    result.AddResult(quiz.Guid, studyUnit!.AnswerState);
            }
            
            foreach (var theory in module.TheoryLessons)
            {
                if (results.TryGetValue(theory.Guid, out var studyUnit))
                    result.AddResult(theory.Guid, studyUnit!.AnswerState);
            }
        }

        return result;
    }
}