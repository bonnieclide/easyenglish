﻿namespace EasyEnglish.Service.StudyService.ViewModels;

public class SetAnswerResultView
{
    public bool Result { get; set; }
}