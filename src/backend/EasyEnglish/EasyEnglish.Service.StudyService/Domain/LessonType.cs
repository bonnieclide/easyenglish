﻿namespace EasyEnglish.Service.StudyService.Domain;

public enum LessonType
{
    Quiz,
    Theory,
    Audio
}