﻿namespace EasyEnglish.Service.StudyService.Domain;

public class CourseLevel
{
    public CourseLevel(string? name)
    {
        Name = name;
    }

    public string? Name { get; private set; }
}