﻿namespace EasyEnglish.Service.StudyService.Domain;

public class Lesson : Entity
{
    public string? Name { get; private set; }
    public string? Description { get; private set; }
    public string? LessonType { get; private set; }
    public bool? IsFree { get; private set; }
    public string? Answer { get; private set; }
    
    public Lesson SetAnswer(string? answer)
    {
        Answer = answer;
        return this;
    }
    
    public Lesson SetName(string? name)
    {
        Name = name;
        return this;
    }

    public Lesson SetIsFree(bool? isFree)
    {
        if (isFree.HasValue)
        {
            IsFree = isFree;
        }
        else
        {
            IsFree = false;
        }
        return this;
    }
    
    public Lesson SetDescription(string? description)
    {
        Description = description;
        return this;
    }
    
    public Lesson SetLessonType(string? lessonType)
    {
        LessonType = lessonType;
        return this;
    }
    
    public Lesson SetGuid(Guid? guid)
    {
        if(guid.HasValue)
            Guid = guid.Value;
        
        return this;
    }
}