﻿namespace EasyEnglish.Service.StudyService.Domain;

public class CourseType
{
    public CourseType(string? name)
    {
        Name = name;
    }

    public string? Name { get; private set; }
}