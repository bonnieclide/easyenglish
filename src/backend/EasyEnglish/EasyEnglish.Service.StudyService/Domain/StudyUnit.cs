﻿namespace EasyEnglish.Service.StudyService.Domain;

public class StudyUnit
{
    public Guid UserGuid { get; private set; }
    public Guid CourseGuid { get; private set; }
    public Guid LessonGuid { get; private set; }
    public bool AnswerState { get; private set; }
    public DateTime Timestamp { get; private set; }

    public StudyUnit SetUserGuid(Guid guid)
    {
        UserGuid = guid;
        return this;
    }
    
    public StudyUnit SetCourseGuid(Guid guid)
    {
        CourseGuid = guid;
        return this;
    }
    
    public StudyUnit SetLessonGuid(Guid guid)
    {
        LessonGuid = guid;
        return this;
    }
    
    public StudyUnit SetAnswerState(bool answerState)
    {
        AnswerState = answerState;
        return this;
    }
    
    public StudyUnit SetTimestamp(DateTime timestamp)
    {
        Timestamp = timestamp;
        return this;
    }
}