﻿namespace EasyEnglish.Service.StudyService.Domain;

public class Course : Entity
{
    public string? Name { get; private set; }
    public string? Description { get; private set; }
    public CourseType? CourseType { get; private set; }
    public CourseLevel? CourseLevel { get; private set; }
    public bool? IsFree { get; private set; }
    public IReadOnlyCollection<Module>? Modules { get; private set; }
    
    public Course AddModules(IReadOnlyCollection<Module>? modules)
    {
        if (modules != null) 
            Modules = new List<Module>(modules);
        
        return this;
    }

    public Course SetName(string? name)
    {
        Name = name;
        return this;
    }

    public Course SetCourseType(string? courseType)
    {
        CourseType = new CourseType(courseType);
        return this;
    }

    public Course SetCourseLevel(string? courseLevel)
    {
        CourseLevel = new CourseLevel(courseLevel);
        return this;
    }

    public Course SetIsFree(bool? isFree)
    {
        if (isFree.HasValue)
        {
            IsFree = isFree;
        }
        else
        {
            IsFree = false;
        }
        return this;
    }
    
    public Course SetDescription(string? description)
    {
        Description = description;
        return this;
    }
    
    public Course SetGuid(Guid? guid)
    {
        if(guid.HasValue)
            Guid = guid.Value;
        
        return this;
    }
}