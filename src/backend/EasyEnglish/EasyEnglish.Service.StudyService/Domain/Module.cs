﻿namespace EasyEnglish.Service.StudyService.Domain;

public class Module : Entity
{
    public string? Name { get; private set; }
    public bool? IsFree { get; private set; }
    public string? Description { get; private set; }
    public IReadOnlyCollection<Lesson> AudioLessons { get; private set; } = new List<Lesson>();
    public IReadOnlyCollection<Lesson> QuizLessons { get; private set; } = new List<Lesson>();
    public IReadOnlyCollection<Lesson> TheoryLessons { get; private set; } = new List<Lesson>();
    
    public Module SetIsFree(bool? isFree)
    {
        IsFree = isFree;
        return this;
    }
    
    public Module SetName(string? name)
    {
        Name = name;
        return this;
    }
    
    public Module SetDescription(string? description)
    {
        Description = description;
        return this;
    }
    
    public Module AddLessons(IReadOnlyCollection<Lesson?>? lessons)
    {
        if (lessons is null)
            return this;
        
        foreach (var group in lessons.GroupBy(l=>l?.LessonType))
        {
            switch (group.FirstOrDefault()?.LessonType?.ToLower())
            {
                case "audio":
                    AudioLessons = AudioLessons.Union(group.ToArray()).ToArray()!;
                    return this;
                case "theory":
                    TheoryLessons = TheoryLessons.Union(group.ToArray()).ToArray()!;
                    return this;
                case "quiz":
                    QuizLessons = QuizLessons.Union(group.ToArray()).ToArray()!;
                    return this;
                default:
                    throw new Exception();
            }
        }

        return this;
    }
    
    public Module SetGuid(Guid? guid)
    {
        if(guid.HasValue)
            Guid = guid.Value;
        
        return this;
    }
}