﻿namespace EasyEnglish.Service.StudyService.Domain;

public abstract class Entity
{
    public Guid Guid { get; set; }
}