﻿namespace EasyEnglish.Service.StudyService.Options;

public class EasyEnglishServicesOptions
{
    public string? ServerAddress { get; set; }
}