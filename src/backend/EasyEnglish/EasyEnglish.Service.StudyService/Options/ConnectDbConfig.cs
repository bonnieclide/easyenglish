﻿namespace EasyEnglish.Service.StudyService.Options;

public class ConnectDbConfig
{
    public string? PostgreConnectionString { get; set; }
}