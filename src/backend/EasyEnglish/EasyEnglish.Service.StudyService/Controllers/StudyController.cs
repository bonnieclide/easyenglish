﻿using EasyEnglish.Service.StudyService.Application.Commands;
using EasyEnglish.Service.StudyService.Application.Queries;
using EasyEnglish.Service.StudyService.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace EasyEnglish.Service.StudyService.Controllers;

[ApiController]
[Route("api/[controller]")]
public class StudyController : Controller
{
    private readonly IMediator _mediator;
    private readonly ILogger<StudyController> _logger;

    public StudyController(ILogger<StudyController> logger, IMediator mediator)
    {
        _logger = logger;
        _mediator = mediator;
    }
    
    [HttpPost]
    [Route(nameof(StartStudy))]
    public async Task<ActionResult<bool>> StartStudy(StartCourseCommand command)
    {
        return await _mediator.Send(command);
    }
    
    [HttpPost]
    [Route(nameof(GetProgress))]
    public async Task<ActionResult<ProgressView>> GetProgress(ProgressStateQuery query)
    {
        return await _mediator.Send(query);
    }
    
    [HttpPost]
    [Route(nameof(SetAnswer))]
    public async Task<ActionResult<SetAnswerResultView>> SetAnswer(SetAnswerCommand command)
    {
        return await _mediator.Send(command);
    }
}
