using System.Reflection;
using EasyEnglish.Platform.Brokers;
using EasyEnglish.Service.StudyService.Application.Services;
using EasyEnglish.Service.StudyService.Infrastructure;
using EasyEnglish.Service.StudyService.Infrastructure.Repositories;
using EasyEnglish.Service.StudyService.Options;
using MediatR;
using StackExchange.Redis;

var builder = WebApplication.CreateBuilder(args);

builder.Services.Configure<ConnectDbConfig>(builder.Configuration.GetSection("ConnectionStrings"));
builder.Services.Configure<EasyEnglishServicesOptions>(builder.Configuration.GetSection("EasyEnglishServicesOptions"));

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddMediatR(Assembly.GetExecutingAssembly());
builder.Services.AddHttpClient();

builder.Services.AddSingleton<ICourseLoaderService, CourseLoaderService>();
builder.Services.AddSingleton<IStartedCourseRepository, StartedCourseRepository>();
builder.Services.AddSingleton<IStudyUnitRepository, StudyUnitRepository>();
builder.Services.AddSingleton<IAnswerChecker, AnswerChecker>();

builder.Host.UseCustomSerilog();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();