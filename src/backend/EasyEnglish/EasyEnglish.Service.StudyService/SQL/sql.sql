drop table if exists startedCourse;
drop table if exists studyUnit;

create table startedCourse
(
    id bigserial primary key,
    userGuid uuid not null,
    courseGuid uuid not null
);

create table studyUnit
(
    id bigserial primary key,
    userGuid uuid not null,
    courseGuid uuid not null,
    lessonGuid uuid not null,
    answerState bool not null,
    timeStamp timestamp without time zone default (now() at time zone 'utc')
);



select * from startedCourse;

select * from studyUnit;

create table test
(
    id bigserial primary key,
    userGuid int not null,
    courseGuid int not null
);

INSERT INTO test (userGuid, courseGuid) VALUES (1, 2), (2, 2), (3, 2), (4, 2), (5, 2);

select * from test;

DELETE FROM test 
WHERE NOT userguid = ANY(Array[1, 2])