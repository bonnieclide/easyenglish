﻿using Dapper;
using EasyEnglish.Service.StudyService.Application.Services;
using EasyEnglish.Service.StudyService.Options;
using Microsoft.Extensions.Options;
using Npgsql;

namespace EasyEnglish.Service.StudyService.Infrastructure.Repositories;

public class StartedCourseRepository : IStartedCourseRepository
{
    private readonly string? _connectionString;
    private readonly SemaphoreSlim _semaphore = new(15, 15);

    public StartedCourseRepository(IOptions<ConnectDbConfig> config)
    {
        _connectionString = config.Value.PostgreConnectionString;
    }

    public async Task<bool> AddCourse(Guid userGuid, Guid courseGuid)
    {
        var insertPattern = @"insert into startedCourse (userGuid, courseGuid)
                              values(@UserGuid, @CourseGuid)";
        
        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                UserGuid = userGuid,
                CourseGuid = courseGuid
            };

            var result = await connection.ExecuteAsync(insertPattern, param);

            return result > 0;
        }
        finally
        {
            _semaphore.Release();
        }
    }
}