﻿using Dapper;
using EasyEnglish.Service.StudyService.Application.Services;
using EasyEnglish.Service.StudyService.Domain;
using EasyEnglish.Service.StudyService.Options;
using Microsoft.Extensions.Options;
using Npgsql;

namespace EasyEnglish.Service.StudyService.Infrastructure.Repositories;

public class StudyUnitRepository : IStudyUnitRepository
{
    private readonly string? _connectionString;
    private readonly SemaphoreSlim _semaphore = new(15, 15);

    public StudyUnitRepository(IOptions<ConnectDbConfig> config)
    {
        _connectionString = config.Value.PostgreConnectionString;
    }

    public async Task<bool> AddStudyUnit(StudyUnit studyUnit)
    {
        var insertPattern = @"insert into studyUnit (userGuid, courseGuid, lessonGuid, answerState)
                              values(@UserGuid, @CourseGuid, @LessonGuid, @AnswerState)";
        
        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                UserGuid = studyUnit.UserGuid,
                CourseGuid = studyUnit.CourseGuid,
                LessonGuid = studyUnit.LessonGuid,
                AnswerState = studyUnit.AnswerState
            };

            var result = await connection.ExecuteAsync(insertPattern, param);

            return result > 0;
        }
        finally
        {
            _semaphore.Release();
        }
    }

    public async Task<StudyUnit[]> GetStudyUnit(Guid userGuid, Guid courseGuid)
    {
        var getPattern = @"select userguid, courseguid, lessonguid, answerstate, timestamp
                           from studyUnit 
                           where userguid = @UserGuid and courseguid = @CourseGuid";
        
        try
        {
            await _semaphore.WaitAsync();

            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync();

            var param = new
            {
                UserGuid = userGuid,
                CourseGuid = courseGuid
            };

            var result = await connection.QueryAsync<StudyUnit>(getPattern, param);

            return result.ToArray();
        }
        finally
        {
            _semaphore.Release();
        }
    }
}